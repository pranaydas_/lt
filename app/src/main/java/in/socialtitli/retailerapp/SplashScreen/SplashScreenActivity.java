package in.socialtitli.retailerapp.SplashScreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import in.socialtitli.retailerapp.AdvertiseScreen.AdvertiseScreen;
import in.socialtitli.retailerapp.Login.LoginActivity;
import in.socialtitli.retailerapp.MainActivity;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.ActivitySplashScreenBinding;

public class SplashScreenActivity extends AppCompatActivity {

    private ActivitySplashScreenBinding binding;
    private Context mCon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);

        mCon = this;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!PreferenceUtil.isUserLoggedIn()) {
                    startActivity(new Intent(mCon, LoginActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(mCon, MainActivity.class));
                    finish();
                }

                //startActivity(new Intent(mCon, AdvertiseScreen.class));
                //startActivity(new Intent(mCon, MainActivity.class));
            }
        }, 3000);
    }
}