package in.socialtitli.retailerapp.ContactAdmin.Model;

import com.google.gson.annotations.SerializedName;

public class FeedbackModel {

    @SerializedName("UserId")
    private String userId;

    @SerializedName("subject")
    private String subject;

    @SerializedName("comment")
    private String comment;

    public FeedbackModel() {
    }

    public FeedbackModel(String userId, String subject, String comment) {
        this.userId = userId;
        this.subject = subject;
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "FeedbackModel{" +
                "userId='" + userId + '\'' +
                ", subject='" + subject + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
