package in.socialtitli.retailerapp.ContactAdmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import in.socialtitli.retailerapp.ContactAdmin.Model.FeedbackModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.ActivityFeedbackBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {
    private Context mCon;
    private ActivityFeedbackBinding binding;
    private String subjectStr, messageStr, userIdStr;
    private FeedbackModel feedbackModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);

        mCon = this;

        if (PreferenceUtil.getUser().getId() != null) {
            userIdStr = PreferenceUtil.getUser().getId();
        }

        binding.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectStr = binding.subjectEditText.getText().toString().trim();
                messageStr = binding.messageEditText.getText().toString().trim();

                validate();
            }
        });
    }

    private void validate() {
        boolean isValidSubject = false, isValidMessage = false;

        if (TextUtils.isEmpty(subjectStr)) {
            binding.subjectTextLayout.setError(mCon.getResources().getString(R.string.cannot_be_empty));
        } else {
            binding.subjectTextLayout.setError(null);
            isValidSubject = true;
        }

        if (TextUtils.isEmpty(messageStr)) {
            binding.messageTextLayout.setError(mCon.getResources().getString(R.string.cannot_be_empty));
        } else {
            binding.messageTextLayout.setError(null);
            isValidMessage = true;
        }

        if (isValidSubject && isValidMessage) {
            feedbackModel = new FeedbackModel(userIdStr, subjectStr, messageStr);
            sendFeedback(feedbackModel);
        }
    }

    private void sendFeedback(FeedbackModel feedbackModel) {
        try {

            Call<String> call = ApiClient.getNetworkService().sendFeedback(feedbackModel);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            String result = response.body();

                            finish();
                            Toast.makeText(mCon, result, Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "sendFeedback: " + e.getMessage());
        }
    }
}