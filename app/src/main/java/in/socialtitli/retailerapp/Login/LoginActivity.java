package in.socialtitli.retailerapp.Login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.ActivityLoginBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityLoginBinding binding;
    private String userNameStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        mCon = this;

        binding.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameStr = binding.userNameEditText.getText().toString().trim();

                validate();
            }
        });
    }

    public void validate() {
        boolean isValidUserName = false;

        if (userNameStr.length() > 0) {
            if (userNameStr.length() < 10) {
                binding.userNameTextLayout.setError("" + getResources().getString(R.string.valid_mobile_no));
            } else if (!userNameStr.startsWith("6") && !userNameStr.startsWith("7") && !userNameStr.startsWith("8") && !userNameStr.startsWith("9")) {
                binding.userNameTextLayout.setError("" + getResources().getString(R.string.strValidNumStart));
            } else {
                isValidUserName = true;
                binding.userNameTextLayout.setError(null);
            }
        } else {
            binding.userNameTextLayout.setError("" + getResources().getString(R.string.cannot_be_empty));
        }

        if (isValidUserName) {
            //loginHeaderModel = new LoginHeaderModel(userNameStr);
            //loadData(loginHeaderModel);
            loadData();
        }
    }

    private void loadData() {
        try {
            Call<String> call = ApiClient.getNetworkService().loginUser(userNameStr);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                   handleResponseCode(response);

                    //Log.d("loginApi", ""+response.body());

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Log.d("check", "" + t.getMessage());
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "loadData: LoginActivity" + e.getMessage());
        }
    }

    private void handleResponseCode(Response<String> response) {
        assert response.body() != null;
        switch (response.body()) {
            case "Invalid mobile number":
                Snackbar.make(binding.loginButton, R.string.invalid_username, Snackbar.LENGTH_SHORT).setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }).show();
                break;

            case "You are not eligible to login":
                Snackbar.make(binding.loginButton, R.string.not_eligible, Snackbar.LENGTH_SHORT).setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }).show();
                break;

            case "otp send to your register number":
                Toast.makeText(mCon, "OTP sent to registered number", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mCon, OtpScreenActivity.class);
                intent.putExtra("userName", userNameStr);
                startActivity(intent);
                finish();
                break;

            default:
                Snackbar.make(binding.loginButton, R.string.something_went_wrong, Snackbar.LENGTH_SHORT).setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }).show();
                break;
        }
    }
}