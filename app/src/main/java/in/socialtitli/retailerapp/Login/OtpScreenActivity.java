package in.socialtitli.retailerapp.Login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.concurrent.TimeUnit;

import in.socialtitli.retailerapp.MainActivity;
import in.socialtitli.retailerapp.Model.ParentDetailsModel;
import in.socialtitli.retailerapp.Model.UserModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityOtpScreenBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpScreenActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityOtpScreenBinding binding;
    private String userNameStr, otpStr;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otp_screen);

        mCon = this;

        userNameStr = getIntent().getStringExtra("userName");

        startTimer();

        binding.verifyOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpStr = binding.otpEditText.getText().toString().trim();

                validate();
            }
        });

        binding.closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitOtpVerification();
            }
        });

        binding.resendOtpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOtp();
            }
        });
    }

    private void validate() {
        boolean isValidOtp = false;

        if (!TextUtils.isEmpty(otpStr)) {
            if (otpStr.length() < 4) {
                binding.otpTextLayout.setError(getResources().getString(R.string.enter_valid_otp));
            } else {
                binding.otpTextLayout.setError(null);
                isValidOtp = true;
            }
        } else {
            binding.otpTextLayout.setError(getResources().getString(R.string.cannot_be_empty));
        }

        if (isValidOtp) {
            sendOtp(userNameStr, otpStr);
        }
    }

    private void startTimer() {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(180000, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                binding.otpTimerTextView.setText("" + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {

                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(500);

                binding.otpTimerTextView.setVisibility(View.GONE);
                binding.resendOtpTextView.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    private void resendOtp() {
        try {
            Call<String> call = ApiClient.getNetworkService().loginUser(PreferenceUtil.getUser().getId());

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(OtpScreenActivity.this, response.body(), Toast.LENGTH_SHORT).show();
                        binding.resendOtpTextView.setVisibility(View.GONE);
                        binding.otpTimerTextView.setVisibility(View.VISIBLE);
                        startTimer();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (!call.isCanceled()) {
                        //Toast.makeText(OtpScreenActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "resendOtp: OtpScreenActivity" + e.getMessage());
        }
    }

    private void sendOtp(String model, String otp) {
        try {
            Call<UserModel> call = ApiClient.getNetworkService().verifyOtp(model, otp);

            call.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    handleResponseCode(response);

                    Log.d("otpApi", ""+response.body());

                }

                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        //Toast.makeText(OtpScreenActivity.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "sendOtp: OtpScreenActivity" + e.getMessage());
        }
    }

    private void handleResponseCode(Response<UserModel> response) {
        switch (response.code()) {
            case ResponseCodes.MOBILE_NUMBER_NOT_REGISTERED:
                Snackbar.make(binding.verifyOtpButton, R.string.invalid_otp, Snackbar.LENGTH_SHORT).setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }).show();

                //Toast.makeText(mCon, "error msg", Toast.LENGTH_SHORT).show();
                break;

            case ResponseCodes.SUCCESS:
                getParentId(response.body().getId());
                Toast.makeText(mCon, R.string.successfully_login, Toast.LENGTH_SHORT).show();

                PreferenceUtil.setUserLoggedIn(true);

                Log.d("check status", String.valueOf(PreferenceUtil.isUserLoggedIn()));

                PreferenceUtil.setUser(response.body());

                Intent intent = new Intent(mCon, MainActivity.class);
                startActivity(intent);
                finish();

                break;

            default:
                Snackbar.make(binding.verifyOtpButton, R.string.something_went_wrong, Snackbar.LENGTH_SHORT).setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }).show();
                break;
        }
    }

    private void getParentId(String retailerId) {
        try {
            Call<ParentDetailsModel> call = ApiClient.getNetworkService().fetchParent(retailerId);

            call.enqueue(new Callback<ParentDetailsModel>() {
                @Override
                public void onResponse(Call<ParentDetailsModel> call, Response<ParentDetailsModel> response) {
                    ParentDetailsModel parent = response.body();

                    PreferenceUtil.setParent(parent);
                }

                @Override
                public void onFailure(Call<ParentDetailsModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        //Toast.makeText(OtpScreenActivity.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "sendOtp: OtpScreenActivity" + e.getMessage());
        }
    }

    private void exitOtpVerification() {
        new MaterialDialog.Builder(mCon)
                .title("Exit Otp Verification")
                .content("Are you sure you want to exit verification process")
                .positiveText("Yes")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .negativeText("No")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        exitOtpVerification();
    }
}