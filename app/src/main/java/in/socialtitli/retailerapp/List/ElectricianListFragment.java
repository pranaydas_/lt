package in.socialtitli.retailerapp.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.List.Activity.AddElectricianActivity;
import in.socialtitli.retailerapp.List.Adapter.ElectricianListAdapter;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.Login.OtpScreenActivity;
import in.socialtitli.retailerapp.Model.ParentDetailsModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.FragmentListBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ElectricianListFragment extends Fragment {

    private static final int REQUEST_CODE = 55;

    private Context mCon;
    private FragmentListBinding binding;

    private BottomSheetDialog filterSheetDialog;
    private ElectricianListAdapter electricianListAdapter;
    private List<ElectricianListModel> electricianListModels;

    private TextView highestPointsTextView, lowestPointsTextView, activeTextView, inActiveTextView;

    private LinearLayoutManager manager;

    private String retailerIdStr, parentIsoId="";

    private MaterialDialog dialog;

    private String requestType;
    //private int position;
    private boolean flag = true;

    private Call<List<ElectricianListModel>> call;


    public ElectricianListFragment() {
        // Required empty public constructor
    }

    public static ElectricianListFragment newInstance() {
        return new ElectricianListFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);

        View view = binding.getRoot();

        mCon = getActivity();
        manager = new LinearLayoutManager(mCon);

        retailerIdStr = PreferenceUtil.getUser().getId();
        getParentId(retailerIdStr);

        filterSheetDialog = new BottomSheetDialog(mCon, R.style.BottomSheetDialog);

        binding.electricianListRecyclerView.setHasFixedSize(true);
        binding.electricianListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.electricianListRecyclerView.setLayoutManager(manager);

        electricianListAdapter = new ElectricianListAdapter(mCon, retailerIdStr);

        binding.addElectricianFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(parentIsoId != null && !(parentIsoId.equalsIgnoreCase(""))){
                    Intent intent = new Intent(mCon, AddElectricianActivity.class);
                    // startActivity(intent);
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    Toast.makeText(mCon, "Retailer not yet mapped to cluster", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.searchTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.searchView.getVisibility() == View.GONE) {
                    binding.searchView.setVisibility(View.VISIBLE);

                    binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return true;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            searchData(newText);
                            return false;
                        }
                    });

                } else {
                    binding.searchView.setVisibility(View.GONE);
                }

            }
        });

        binding.filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterBottomSheet();
                filterSheetDialog.show();
            }
        });

        loadElectricianData();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //pageIndex = 0;
            loadElectricianData();
            Log.d("check ", " onActivityResult: ");
        }
    }

    private void loadElectricianData() {
        try {
            call = ApiClient.getNetworkService().fetchingElectricianList(retailerIdStr);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.getting_electrician)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<List<ElectricianListModel>>() {
                @Override
                public void onResponse(Call<List<ElectricianListModel>> call, Response<List<ElectricianListModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            if (response.body() != null) {
                                electricianListModels = response.body();

                                binding.errorLinear.setVisibility(View.GONE);

                                binding.totalTextView.setVisibility(View.VISIBLE);
                                binding.totalTextView.setText(getResources().getString(R.string.total_no_of_electrician) + " " + electricianListModels.size());

                                binding.electricianListRecyclerView.setVisibility(View.VISIBLE);
                                electricianListAdapter.addAll(electricianListModels);
                                binding.electricianListRecyclerView.setAdapter(electricianListAdapter);
                                electricianListAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            } else {
                                binding.totalTextView.setVisibility(View.GONE);
                                binding.electricianListRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.data_not_found);
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        /*dialogDisplay = false;
                        pageIndex = pageIndex + 10000;
                        loadElectricianData();*/

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            try {
                                binding.totalTextView.setVisibility(View.GONE);
                                binding.electricianListRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.no_data);
                                dialog.dismiss();
                                //Toast.makeText(mCon, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            binding.totalTextView.setVisibility(View.GONE);
                            binding.electricianListRecyclerView.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(R.string.data_not_found);
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        binding.totalTextView.setVisibility(View.GONE);
                        binding.electricianListRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<ElectricianListModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        binding.totalTextView.setVisibility(View.GONE);
                        binding.electricianListRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "loadElectricianData: ElectricianListFragment" + e.getMessage());
        }
    }

    private void searchData(String input) {
        List<ElectricianListModel> list = new ArrayList();

        for (ElectricianListModel model : electricianListModels) {
            if (model.getElectricianFullName().toLowerCase().contains(input.toLowerCase()) ||
                    model.getUserBalance().equals(input) || model.getUserTotalBalance().equals(input) ||
                    model.getUsername().equals(input)) {
                list.add(model);
            }
        }

        binding.totalTextView.setText(getResources().getString(R.string.total_no_of_electrician) + " " + list.size());

        electricianListAdapter.filterList(list);
    }

    private void filterBottomSheet() {
        View sheetView = Objects.requireNonNull(getLayoutInflater().inflate(R.layout.bottomsheet_filters, null));
        filterSheetDialog.setContentView(sheetView);
        filterSheetDialog.setCanceledOnTouchOutside(false);

        AppCompatImageView closeImageView = sheetView.findViewById(R.id.closeImageView);
        highestPointsTextView = sheetView.findViewById(R.id.highestPointsTextView);
        lowestPointsTextView = sheetView.findViewById(R.id.lowestPointsTextView);
        activeTextView = sheetView.findViewById(R.id.activeTextView);
        inActiveTextView = sheetView.findViewById(R.id.inActiveTextView);

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterSheetDialog.cancel();
            }
        });

        highestPointsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                electricianListAdapter.clearFilter();
                binding.electricianListRecyclerView.setAdapter(null);

                requestType = "highest";

                //loadFilteredData(requestType);
                filterSheetDialog.cancel();
            }
        });

        lowestPointsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                electricianListAdapter.clearFilter();
                binding.electricianListRecyclerView.setAdapter(null);

                requestType = "lowest";

                // loadFilteredData(requestType);
                filterSheetDialog.cancel();
            }
        });

        activeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                electricianListAdapter.clearFilter();
                binding.electricianListRecyclerView.setAdapter(null);

                requestType = "active";

                // loadFilteredData(requestType);
                filterSheetDialog.cancel();
            }
        });

        inActiveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                electricianListAdapter.clearFilter();
                binding.electricianListRecyclerView.setAdapter(null);

                requestType = "inactive";

                // loadFilteredData(requestType);
                filterSheetDialog.cancel();
            }
        });
    }

    private void getParentId(String retailerId) {
        try {
            Call<ParentDetailsModel> call = ApiClient.getNetworkService().fetchParent(retailerId);

            call.enqueue(new Callback<ParentDetailsModel>() {
                @Override
                public void onResponse(Call<ParentDetailsModel> call, Response<ParentDetailsModel> response) {
                    ParentDetailsModel parent = response.body();

                    PreferenceUtil.setParent(parent);

                    parentIsoId = parent.getIsoId();
                }

                @Override
                public void onFailure(Call<ParentDetailsModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        //Toast.makeText(mCon, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "sendOtp: OtpScreenActivity" + e.getMessage());
        }
    }

//    private void loadFilteredData(String filterType) {
//        try {
//            Call<List<ElectricianListModel>> call = ApiClient.getNetworkService().getFilterData(PreferenceUtil.getUser().getId(), filterType);
//
//            /*if (filterDialog == true) {*/
//            dialog = new MaterialDialog.Builder(mCon)
//                    .title(R.string.getting_electrician)
//                    .content(R.string.loading)
//                    .canceledOnTouchOutside(false)
//                    .progress(true, 0)
//                    .widgetColorRes(R.color.colorPrimary)
//                    .show();
//            //}
//
//            call.enqueue(new Callback<List<ElectricianListModel>>() {
//                @Override
//                public void onResponse(Call<List<ElectricianListModel>> call, Response<List<ElectricianListModel>> response) {
//                    if (response.isSuccessful()) {
//                        if (response.code() == ResponseCodes.SUCCESS) {
//
//                            if (response.body() != null) {
//                                electricianListModels = response.body();
//
//                                //PreferenceUtil.setFilterType(filterType);
//                                binding.totalTextView.setVisibility(View.VISIBLE);
//                                binding.totalTextView.setText(getResources().getString(R.string.total_no_of_electrician) + " " + electricianListModels.size());
//
//                                binding.errorLinear.setVisibility(View.GONE);
//                                binding.electricianListRecyclerView.setVisibility(View.VISIBLE);
//                                electricianListAdapter.addAllFilter(electricianListModels);
//                                binding.electricianListRecyclerView.setAdapter(electricianListAdapter);
//                                dialog.dismiss();
//
//                            } else {
//                                binding.totalTextView.setVisibility(View.GONE);
//                                binding.electricianListRecyclerView.setVisibility(View.GONE);
//                                binding.errorLinear.setVisibility(View.VISIBLE);
//                                binding.errorTextView.setText(R.string.data_not_found);
//                                Toast.makeText(mCon, getString(R.string.data_not_found), Toast.LENGTH_SHORT).show();
//                                dialog.dismiss();
//                            }
//
//                        /*filterDialog = false;
//                        highestPageIndex = highestPageIndex + 10000;
//                        loadFilteredData(PreferenceUtil.getFilterType());*/
//
//                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
//                            binding.totalTextView.setVisibility(View.GONE);
//                            //Toast.makeText(mCon, getString(R.string.end_of_list), Toast.LENGTH_SHORT).show();
//                            binding.electricianListRecyclerView.setVisibility(View.GONE);
//                            binding.errorLinear.setVisibility(View.VISIBLE);
//                            binding.errorTextView.setText(R.string.no_data);
//                            dialog.dismiss();
//
//                        } else {
//                            binding.totalTextView.setVisibility(View.GONE);
//                            binding.electricianListRecyclerView.setVisibility(View.GONE);
//                            binding.errorLinear.setVisibility(View.VISIBLE);
//                            binding.errorTextView.setText(R.string.data_not_found);
//                            Toast.makeText(mCon, getString(R.string.data_not_found), Toast.LENGTH_SHORT).show();
//                            dialog.dismiss();
//                        }
//                    } else {
//                        binding.totalTextView.setVisibility(View.GONE);
//                        binding.electricianListRecyclerView.setVisibility(View.GONE);
//                        binding.errorLinear.setVisibility(View.VISIBLE);
//                        binding.errorTextView.setText(R.string.something_went_wrong);
//                        Toast.makeText(mCon, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
//                        dialog.dismiss();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<List<ElectricianListModel>> call, Throwable t) {
//                    if (!call.isCanceled()) {
//                        binding.electricianListRecyclerView.setVisibility(View.GONE);
//                        binding.errorLinear.setVisibility(View.VISIBLE);
//                        binding.errorTextView.setText(R.string.something_went_wrong);
//                        Toast.makeText(mCon, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
//                        dialog.dismiss();
//                    }
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.d("check", "loadFilteredData: ElectricianListFragment" + e.getMessage());
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("check", "detach");
    }

}