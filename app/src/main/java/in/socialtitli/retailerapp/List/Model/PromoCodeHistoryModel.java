package in.socialtitli.retailerapp.List.Model;

import com.google.gson.annotations.SerializedName;

public class PromoCodeHistoryModel {

    @SerializedName("id")
    private String id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("code")
    private String code;

    @SerializedName("point")
    private String point;

    @SerializedName("type")
    private String type;

    @SerializedName("note")
    private String note;

    @SerializedName("category_type")
    private String category_type;

    @SerializedName("created_by")
    private String created_by;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("productName")
    private String productName;

    public PromoCodeHistoryModel() {
    }

    public PromoCodeHistoryModel(String id, String user_id, String code, String point, String type, String note, String category_type, String created_by, String created_at, String productName) {
        this.id = id;
        this.user_id = user_id;
        this.code = code;
        this.point = point;
        this.type = type;
        this.note = note;
        this.category_type = category_type;
        this.created_by = created_by;
        this.created_at = created_at;
        this.productName = productName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCategory_type() {
        return category_type;
    }

    public void setCategory_type(String category_type) {
        this.category_type = category_type;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "PromoCodeHistoryModel{" +
                "id='" + id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", code='" + code + '\'' +
                ", point='" + point + '\'' +
                ", type='" + type + '\'' +
                ", note='" + note + '\'' +
                ", category_type='" + category_type + '\'' +
                ", created_by='" + created_by + '\'' +
                ", created_at='" + created_at + '\'' +
                ", productName='" + productName + '\'' +
                '}';
    }
}
