package in.socialtitli.retailerapp.List.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.List.Model.AddElectricianModel;
import in.socialtitli.retailerapp.Model.CityList;
import in.socialtitli.retailerapp.Model.StateList;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.ActivityAddElectricianBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddElectricianActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityAddElectricianBinding binding;
    private String userNameStr, firstNameStr, lastNameStr, emailStr, stateStr, cityStr, address1Str, address2Str,
            address3Str, pincodeStr, parentIsoId, retailerId, otherCityStr;
    private List<String> states, cities;
    private int statePos, cityPos, selId, ctyId;
    private List<Integer> statesIdDb, citiesIdDb;
    ArrayAdapter stateAdapter, cityAdapter;
    private List<StateList> stateList = new ArrayList<>();
    private List<CityList> cityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_electrician);

        mCon = this;
        loadState();

        retailerId = PreferenceUtil.getUser().getId();
        parentIsoId = PreferenceUtil.getParent().getIsoId();

        binding.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameStr = binding.userNameEditText.getText().toString().trim();
                firstNameStr = binding.firstNameEditText.getText().toString().trim();
                lastNameStr = binding.lastNameEditText.getText().toString().trim();
                pincodeStr = binding.pincodeEditText.getText().toString().trim();

                if (binding.otherTextLayout.getVisibility() == View.VISIBLE) {
                    otherCityStr = binding.otherEditText.getText().toString().trim();
                }

                validate();
            }
        });
    }

    private void loadState() {
        try {
            Call<List<StateList>> call = ApiClient.getNetworkService().fetchStates();

            call.enqueue(new Callback<List<StateList>>() {
                @Override
                public void onResponse(Call<List<StateList>> call, Response<List<StateList>> response) {
                    if (response.isSuccessful()) {
                        stateList = response.body();

                        if (stateList != null) {

                            states = new ArrayList<>();
                            statesIdDb = new ArrayList<>();
                            states.add("Select State");
                            statesIdDb.add(0);

                            for (StateList model : stateList) {
                                states.add(model.getStateName());
                                statesIdDb.add(model.getStateId());
                            }

                            stateAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, states);
                            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.stateSpinner.setAdapter(stateAdapter);

                            binding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    stateStr = binding.stateSpinner.getSelectedItem().toString().trim();
                                    statePos = binding.stateSpinner.getSelectedItemPosition();

                                    selId = statesIdDb.get(statePos);

                                    loadCities(selId);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<StateList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCities(int stateId) {
        try {
            Call<List<CityList>> call = ApiClient.getNetworkService().fetchCities(stateId);

            call.enqueue(new Callback<List<CityList>>() {
                @Override
                public void onResponse(Call<List<CityList>> call, Response<List<CityList>> response) {
                    if (response.isSuccessful()) {
                        cityList = response.body();

                        if (cityList != null) {

                            cities = new ArrayList<>();
                            citiesIdDb = new ArrayList<>();
                            cities.add("Select City");
                            citiesIdDb.add(0);

                            for (CityList model : cityList) {
                                cities.add(model.getCityName());
                                citiesIdDb.add(model.getCityId());
                            }

                            cityAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, cities);
                            cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.citySpinner.setAdapter(cityAdapter);

                            binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    cityStr = binding.citySpinner.getSelectedItem().toString();
                                    cityPos = binding.citySpinner.getSelectedItemPosition();

                                    if (cityStr.equals("Other")) {
                                        binding.otherTextLayout.setVisibility(View.VISIBLE);
                                    } else {
                                        binding.otherTextLayout.setVisibility(View.GONE);
                                    }

                                    ctyId = citiesIdDb.get(cityPos);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    }
                }

                @Override
                public void onFailure(Call<List<CityList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validate() {
        boolean isValidUserName = false, isValidFirstName = false, isValidLastName = false,
                isValidCity = false, isValidState = false, isValidPincode = false;

        if (userNameStr.length() > 0) {
            if (userNameStr.length() < 10) {
                binding.userNameTextLayout.setError("" + getResources().getString(R.string.valid_mobile_no));
            } else if (!userNameStr.startsWith("6") && !userNameStr.startsWith("7") && !userNameStr.startsWith("8") && !userNameStr.startsWith("9")) {
                binding.userNameTextLayout.setError("" + getResources().getString(R.string.strValidNumStart));
            } else {
                isValidUserName = true;
                binding.userNameTextLayout.setError(null);
            }
        } else {
            binding.userNameTextLayout.setError("" + getResources().getString(R.string.cannot_be_empty));
        }

        if (TextUtils.isEmpty(firstNameStr)) {
            binding.firstNameTextLayout.setError(mCon.getResources().getString(R.string.cannot_be_empty));
        } else {
            binding.firstNameTextLayout.setError(null);
            isValidFirstName = true;
        }

        if (TextUtils.isEmpty(lastNameStr)) {
            binding.lastNameTextLayout.setError(mCon.getResources().getString(R.string.cannot_be_empty));
        } else {
            binding.lastNameTextLayout.setError(null);
            isValidLastName = true;
        }


        //Pincode
        if (pincodeStr.length() > 0) {
            if (pincodeStr.length() < 6) {
                binding.pincodeTextLayout.setError(mCon.getResources().getString(R.string.enter_valid_pincode));
            } else {
                isValidPincode = true;
                binding.pincodeTextLayout.setError(null);
            }
        } else {
            binding.pincodeTextLayout.setError(mCon.getResources().getString(R.string.cannot_be_empty));
        }

        if (ctyId != -1) {
            isValidCity = true;
        } else {
            Toast.makeText(mCon, "Invalid City", Toast.LENGTH_SHORT).show();
        }

        if (selId != 0) {
            isValidState = true;
        } else {
            Toast.makeText(mCon, "Invalid State", Toast.LENGTH_SHORT).show();
        }

        if (isValidUserName && isValidFirstName && isValidLastName && isValidCity && isValidState && isValidPincode) {

            addElectricianApi(new AddElectricianModel(parentIsoId, retailerId, userNameStr, firstNameStr, lastNameStr, selId, ctyId, pincodeStr));
        }
    }

    private void addElectricianApi(AddElectricianModel addElectricianModel) {
        try {
            Call<String> call = ApiClient.getNetworkService().addElectrician(addElectricianModel);

            final MaterialDialog dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.electrician)
                    .content(R.string.adding)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            binding.userNameTextLayout.setError(null);
                            Toast.makeText(mCon, response.body(), Toast.LENGTH_SHORT).show();
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 403) {
                        binding.userNameTextLayout.setError(getResources().getString(R.string.username_already_exist));
                        Toast.makeText(mCon, R.string.username_already_exist, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "addElectricianApi: AddElectricianActivity " + e.getMessage());
        }
    }
}