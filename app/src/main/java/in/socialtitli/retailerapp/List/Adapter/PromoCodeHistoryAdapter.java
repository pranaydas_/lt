package in.socialtitli.retailerapp.List.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.List.Model.PromoCodeHistoryModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.PromoCodeHistoryRowBinding;

public class PromoCodeHistoryAdapter extends RecyclerView.Adapter<PromoCodeHistoryAdapter.MyViewHolder> {
    private Context mCon;
    private LayoutInflater inflater;
    private List<PromoCodeHistoryModel> data;

    public PromoCodeHistoryAdapter(Context context) {
        mCon = context;
        inflater = LayoutInflater.from(mCon);
        data = new ArrayList<>();
    }

    public void addList(List<PromoCodeHistoryModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PromoCodeHistoryRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.promo_code_history_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final PromoCodeHistoryModel current = data.get(position);

        holder.binding.idTextView.setText(current.getId());
        holder.binding.codeTextView.setText(current.getCode());
        holder.binding.transactionByTextView.setText(current.getCreated_by());
        holder.binding.createdAtTextView.setText(current.getCreated_at());
        holder.binding.pointTextView.setText(current.getPoint());
        holder.binding.productTextView.setText(current.getProductName());

        if (current.getType() != null) {
            if (current.getType().equals("1")) {
                holder.binding.typeTextView.setText(mCon.getResources().getString(R.string.deposit));
            } else if (current.getType().equals("2")) {
                holder.binding.typeTextView.setText(mCon.getResources().getString(R.string.withdraw));
            }
        }


        holder.binding.categoryTextView.setText(current.getCategory_type());
        holder.binding.noteTextView.setText(current.getNote());

        holder.binding.promoHistoryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.binding.linear.getVisibility() == View.GONE) {
                    expand(holder.binding.linear);
                } else {
                    collapse(holder.binding.linear);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final PromoCodeHistoryRowBinding binding;

        public MyViewHolder(PromoCodeHistoryRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    private static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
