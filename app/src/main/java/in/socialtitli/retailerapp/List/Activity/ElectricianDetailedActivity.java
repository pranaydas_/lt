package in.socialtitli.retailerapp.List.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Objects;

import in.socialtitli.retailerapp.List.Model.ElectricianDetailModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.ActivityElectricianDetailedBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ElectricianDetailedActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityElectricianDetailedBinding binding;
    private String electId, retailerId;
    private ElectricianDetailModel electricianDetailModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_electrician_detailed);

        mCon = this;
        electId = getIntent().getStringExtra("eleId");

        if (getIntent().getStringExtra("retailerId") != null) {
            retailerId = getIntent().getStringExtra("retailerId");
        } else {
            retailerId = PreferenceUtil.getUser().getId();
        }

        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        binding.basicInfoRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.basicInfoLinear.getVisibility() == View.GONE) {
                    expand(binding.basicInfoLinear);
                    binding.imageView1.animate().rotation(180).start();
                } else {
                    collapse(binding.basicInfoLinear);
                    binding.imageView1.animate().rotation(0).start();
                }
            }
        });

        binding.primaryInfoRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.primaryInfoLinear.getVisibility() == View.GONE) {
                    expand(binding.primaryInfoLinear);
                    binding.imageView2.animate().rotation(180).start();
                } else {
                    collapse(binding.primaryInfoLinear);
                    binding.imageView2.animate().rotation(0).start();
                }
            }
        });

        binding.alternateInfoRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.alternateInfoLinear.getVisibility() == View.GONE) {
                    expand(binding.alternateInfoLinear);
                    binding.imageView3.animate().rotation(180).start();
                } else {
                    collapse(binding.alternateInfoLinear);
                    binding.imageView3.animate().rotation(0).start();
                }
            }
        });

        binding.memberInfoRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.memberInfoLinear.getVisibility() == View.GONE) {
                    expand(binding.memberInfoLinear);
                    binding.imageView4.animate().rotation(180).start();
                } else {
                    collapse(binding.memberInfoLinear);
                    binding.imageView4.animate().rotation(0).start();
                }
            }
        });

        getDetailedData();
    }

    private void getDetailedData() {
        try {
            Call<ElectricianDetailModel> call = ApiClient.getNetworkService().fetchingElectricianDetail(retailerId, electId);

            final MaterialDialog dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.electrician_detail)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();

            call.enqueue(new Callback<ElectricianDetailModel>() {
                @Override
                public void onResponse(Call<ElectricianDetailModel> call, Response<ElectricianDetailModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            electricianDetailModel = response.body();

                            binding.username.setText(electricianDetailModel.getUsername());
                            binding.firstName.setText(electricianDetailModel.getFirst_name());
                            binding.lastName.setText(electricianDetailModel.getLast_name());
                            binding.email.setText(electricianDetailModel.getEmail());
                            binding.adharNo.setText(electricianDetailModel.getId_proof());
                            binding.profession.setText(electricianDetailModel.getProfession());
                            binding.qualification.setText(electricianDetailModel.getProfessional_qualification());
                            binding.state.setText(electricianDetailModel.getState());
                            if (electricianDetailModel.getCity() == null) {
                                binding.city.setText(electricianDetailModel.getOtherCity());
                            } else {
                                binding.city.setText(electricianDetailModel.getCity());
                            }
                            binding.address1.setText(electricianDetailModel.getAddress_line_1());
                            binding.address2.setText(electricianDetailModel.getAddress_line_2());
                            binding.address3.setText(electricianDetailModel.getAddress_line_3());
                            binding.pincode.setText(electricianDetailModel.getPincode());

                            binding.connectionType.setText(electricianDetailModel.getPrimary_mobile_connection_type());
                            binding.operator.setText(electricianDetailModel.getPrimary_mobile_operator());
                            binding.circle.setText(electricianDetailModel.getPrimary_mobile_circle());
                            binding.dcn.setText(electricianDetailModel.getPrimary_mobile_dnc());

                            binding.mobileNo.setText(electricianDetailModel.getAlternate_mobile_number());
                            binding.aConnectionType.setText(electricianDetailModel.getAlternate_mobile_connection_type());
                            binding.aOperator.setText(electricianDetailModel.getAlternate_mobile_operator());
                            binding.aCircle.setText(electricianDetailModel.getAlternate_mobile_circle());
                            binding.aDcn.setText(electricianDetailModel.getAlternate_mobile_dnc());

                        /*if(electricianDetailModel.getAlternate_mobile_dnc() != null){
                            if(electricianDetailModel.getAlternate_mobile_dnc().equals("1")){
                                binding.aDcn.setText(getResources().getString(R.string.can_call));
                            }else if(electricianDetailModel.getAlternate_mobile_dnc().equals("2")){
                                binding.aDcn.setText(getResources().getString(R.string.do_not_call));
                            }else {
                                binding.aDcn.setText(null);
                            }
                        }*/

                            binding.membershipLevel.setText(electricianDetailModel.getLevel_id());
                            binding.membershipUpdatedDate.setText(electricianDetailModel.getStart_date());
                            binding.totalPoints.setText(electricianDetailModel.getUser_total_balance());
                            binding.registrationDate.setText(electricianDetailModel.getCreated_at());
                            binding.pointsRedeemed.setText(electricianDetailModel.getUser_redeem_balance());
                            binding.pointsBalance.setText(electricianDetailModel.getUser_balance());
                            binding.rechargeNumber.setText(electricianDetailModel.getRecharge_number());

                            binding.pointHistoryTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(mCon, PointsHistoryActivity.class);
                                    intent.putExtra("retailerId", retailerId);
                                    intent.putExtra("electId", electId);
                                    intent.putExtra("memLevel", electricianDetailModel.getLevel_id());
                                    intent.putExtra("memUpdate", electricianDetailModel.getStart_date());
                                    intent.putExtra("memTotalPoint", electricianDetailModel.getUser_total_balance());
                                    intent.putExtra("memRegDate", electricianDetailModel.getCreated_at());
                                    intent.putExtra("pointsRedeem", electricianDetailModel.getUser_redeem_balance());
                                    intent.putExtra("pointsBalance", electricianDetailModel.getUser_balance());
                                    startActivity(intent);
                                }
                            });

                        } else {
                            Toast.makeText(mCon, "No Data Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<ElectricianDetailModel> call, Throwable t) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "getDetailedData: ElectricianDetailedActivity" + e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            getDetailedData();
        }
    }

    private static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}