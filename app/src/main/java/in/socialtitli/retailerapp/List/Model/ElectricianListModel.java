package in.socialtitli.retailerapp.List.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElectricianListModel {
    @SerializedName("id")
    private String id;

    @SerializedName("electricianFullName")
    private String electricianFullName;

    @SerializedName("username")
    private String username;

    @SerializedName("user_total_balance")
    private String userTotalBalance;

    @SerializedName("user_redeem_balance")
    private String userRedeemBalance;

    @SerializedName("user_balance")
    private String userBalance;

    public ElectricianListModel(){}

    public ElectricianListModel(String id, String electricianFullName, String username, String userTotalBalance, String userRedeemBalance, String userBalance) {
        this.id = id;
        this.electricianFullName = electricianFullName;
        this.username = username;
        this.userTotalBalance = userTotalBalance;
        this.userRedeemBalance = userRedeemBalance;
        this.userBalance = userBalance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getElectricianFullName() {
        return electricianFullName;
    }

    public void setElectricianFullName(String electricianFullName) {
        this.electricianFullName = electricianFullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserTotalBalance() {
        return userTotalBalance;
    }

    public void setUserTotalBalance(String userTotalBalance) {
        this.userTotalBalance = userTotalBalance;
    }

    public String getUserRedeemBalance() {
        return userRedeemBalance;
    }

    public void setUserRedeemBalance(String userRedeemBalance) {
        this.userRedeemBalance = userRedeemBalance;
    }

    public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }
}
