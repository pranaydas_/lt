package in.socialtitli.retailerapp.List.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.List.Activity.ElectricianDetailedActivity;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.ElectricianRowBinding;

public class ElectricianListAdapter extends RecyclerView.Adapter<ElectricianListAdapter.MyViewHolder> {
    private Context mCon;
    private List<ElectricianListModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public ElectricianListAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<ElectricianListModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void addFilter(List<ElectricianListModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void addAllFilter(List<ElectricianListModel> list) {
        if (data != null) {
            //clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clearFilter() {
        int size = data.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                data.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ElectricianRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.electrician_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ElectricianListModel current = data.get(position);

        holder.binding.electricianNameTextVew.setText(current.getElectricianFullName());
        holder.binding.pointsTextView.setText(current.getUserTotalBalance());
        holder.binding.balancePointsTextView.setText(current.getUserBalance());
        holder.binding.mobileNoTextVew.setText(current.getUsername());
        holder.binding.callElectrician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = current.getUsername();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                mCon.startActivity(intent);
            }
        });

        holder.binding.electricianListCardView.setOnClickListener(view -> {
            Intent intent = new Intent(mCon, ElectricianDetailedActivity.class);
            intent.putExtra("retailerId", retailerId);
            intent.putExtra("eleId", current.getId());
            mCon.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void filterList(List<ElectricianListModel> list) {
        data = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ElectricianRowBinding binding;

        public MyViewHolder(ElectricianRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
