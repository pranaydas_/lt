package in.socialtitli.retailerapp.List.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.List.Adapter.PromoCodeHistoryAdapter;
import in.socialtitli.retailerapp.List.Model.PromoCodeHistoryModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.ActivityPointsHistoryBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointsHistoryActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityPointsHistoryBinding binding;
    private List<PromoCodeHistoryModel> promoCodeHistoryModelList;
    private PromoCodeHistoryAdapter promoCodeHistoryAdapter;

    private String retailerId, electId, memLevel, memUpdate, memTotalPoint, memRegDate, pointsRedeem, pointsBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_points_history);

        mCon = this;

        electId = getIntent().getStringExtra("electId");
        memLevel = getIntent().getStringExtra("memLevel");
        memUpdate = getIntent().getStringExtra("memUpdate");
        memTotalPoint = getIntent().getStringExtra("memTotalPoint");
        memRegDate = getIntent().getStringExtra("memRegDate");
        pointsRedeem = getIntent().getStringExtra("pointsRedeem");
        pointsBalance = getIntent().getStringExtra("pointsBalance");

        if (getIntent().getStringExtra("retailerId") != null) {
            retailerId = getIntent().getStringExtra("retailerId");
        } else {
            retailerId = PreferenceUtil.getUser().getId();
        }

        if (memLevel != null) {
            binding.membershipLevelTextView.setText(memLevel);
        } else {
            binding.membershipLevelTextView.setText(getResources().getString(R.string.na));
        }

        if (memUpdate != null) {
            binding.membershipUpdatedDateTextView.setText(memUpdate);
        } else {
            binding.membershipUpdatedDateTextView.setText(getResources().getString(R.string.na));
        }

        if (memTotalPoint != null) {
            binding.totalPointsTextView.setText(memTotalPoint);
        } else {
            binding.totalPointsTextView.setText("0");
        }

        if (memRegDate != null) {
            binding.registrationDateTextView.setText(memRegDate);
        } else {
            binding.registrationDateTextView.setText(getResources().getString(R.string.na));
        }

        if (pointsRedeem != null) {
            binding.pointsRedeemedTextView.setText(pointsRedeem);
        } else {
            binding.pointsRedeemedTextView.setText("0");
        }

        if (pointsBalance != null) {
            binding.pointsBalanceTextView.setText(pointsBalance);
        } else {
            binding.pointsBalanceTextView.setText("0");
        }

        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        binding.memberInfoRecyclerView.setHasFixedSize(true);
        binding.memberInfoRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.memberInfoRecyclerView.setLayoutManager(new LinearLayoutManager(mCon));

        promoCodeHistoryAdapter = new PromoCodeHistoryAdapter(mCon);

        //ElectricianHeaderModel electricianHeaderModel = new ElectricianHeaderModel(isoId, electId);

        getDetailedData();
    }

    private void getDetailedData() {
        try {
            Call<List<PromoCodeHistoryModel>> call = ApiClient.getNetworkService().fetchingPromoCodeHistory(retailerId, electId);

            final MaterialDialog dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.point_history)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();


            call.enqueue(new Callback<List<PromoCodeHistoryModel>>() {
                @Override
                public void onResponse(Call<List<PromoCodeHistoryModel>> call, Response<List<PromoCodeHistoryModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            promoCodeHistoryModelList = response.body();

                            promoCodeHistoryAdapter.addList(promoCodeHistoryModelList);
                            binding.memberInfoRecyclerView.setAdapter(promoCodeHistoryAdapter);
                        } else {
                            Toast.makeText(mCon, "No Data Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<PromoCodeHistoryModel>> call, Throwable t) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "getDetailedData: PointsHistoryActivity" + e.getMessage());
        }
    }
}