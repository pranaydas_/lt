package in.socialtitli.retailerapp.List.Model;

import com.google.gson.annotations.SerializedName;

public class ElectricianDetailModel {

    @SerializedName("id")
    private String id;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("profession")
    private String profession;

    @SerializedName("professional_qualification")
    private String professional_qualification;

    @SerializedName("state")
    private String state;

    @SerializedName("city")
    private String city;

    @SerializedName("address_line_1")
    private String address_line_1;

    @SerializedName("address_line_2")
    private String address_line_2;

    @SerializedName("address_line_3")
    private String address_line_3;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("primary_mobile_connection_type")
    private String primary_mobile_connection_type;

    @SerializedName("primary_mobile_operator")
    private String primary_mobile_operator;

    @SerializedName("primary_mobile_circle")
    private String primary_mobile_circle;

    @SerializedName("primary_mobile_dnc")
    private String primary_mobile_dnc;

    @SerializedName("alternate_mobile_number")
    private String alternate_mobile_number;

    @SerializedName("alternate_mobile_connection_type")
    private String alternate_mobile_connection_type;

    @SerializedName("alternate_mobile_operator")
    private String alternate_mobile_operator;

    @SerializedName("alternate_mobile_circle")
    private String alternate_mobile_circle;

    @SerializedName("alternate_mobile_dnc")
    private String alternate_mobile_dnc;

    @SerializedName("recharge_number")
    private String recharge_number;

    @SerializedName("created_by")
    private String created_by;

    @SerializedName("updated_by")
    private String updated_by;

    @SerializedName("level_id")
    private String level_id;

    @SerializedName("other_city")
    private String otherCity;

    @SerializedName("id_proof")
    private String id_proof;

    @SerializedName("primary_verify")
    private String primary_verify;

    @SerializedName("alternate_verify")
    private String alternate_verify;

    @SerializedName("user_total_balance")
    private String user_total_balance;

    @SerializedName("user_redeem_balance")
    private String user_redeem_balance;

    @SerializedName("user_balance")
    private String user_balance;

    @SerializedName("start_date")
    private String start_date;

    public ElectricianDetailModel() {
    }

    public ElectricianDetailModel(String id, String first_name, String last_name, String username, String email, String created_at, String updated_at, String profession, String professional_qualification, String state, String city, String address_line_1, String address_line_2, String address_line_3, String pincode, String primary_mobile_connection_type, String primary_mobile_operator, String primary_mobile_circle, String primary_mobile_dnc, String alternate_mobile_number, String alternate_mobile_connection_type, String alternate_mobile_operator, String alternate_mobile_circle, String alternate_mobile_dnc, String recharge_number, String created_by, String updated_by, String level_id, String otherCity, String id_proof, String primary_verify, String alternate_verify, String user_total_balance, String user_redeem_balance, String user_balance, String start_date) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.email = email;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.profession = profession;
        this.professional_qualification = professional_qualification;
        this.state = state;
        this.city = city;
        this.address_line_1 = address_line_1;
        this.address_line_2 = address_line_2;
        this.address_line_3 = address_line_3;
        this.pincode = pincode;
        this.primary_mobile_connection_type = primary_mobile_connection_type;
        this.primary_mobile_operator = primary_mobile_operator;
        this.primary_mobile_circle = primary_mobile_circle;
        this.primary_mobile_dnc = primary_mobile_dnc;
        this.alternate_mobile_number = alternate_mobile_number;
        this.alternate_mobile_connection_type = alternate_mobile_connection_type;
        this.alternate_mobile_operator = alternate_mobile_operator;
        this.alternate_mobile_circle = alternate_mobile_circle;
        this.alternate_mobile_dnc = alternate_mobile_dnc;
        this.recharge_number = recharge_number;
        this.created_by = created_by;
        this.updated_by = updated_by;
        this.level_id = level_id;
        this.otherCity = otherCity;
        this.id_proof = id_proof;
        this.primary_verify = primary_verify;
        this.alternate_verify = alternate_verify;
        this.user_total_balance = user_total_balance;
        this.user_redeem_balance = user_redeem_balance;
        this.user_balance = user_balance;
        this.start_date = start_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getProfessional_qualification() {
        return professional_qualification;
    }

    public void setProfessional_qualification(String professional_qualification) {
        this.professional_qualification = professional_qualification;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public void setAddress_line_2(String address_line_2) {
        this.address_line_2 = address_line_2;
    }

    public String getAddress_line_3() {
        return address_line_3;
    }

    public void setAddress_line_3(String address_line_3) {
        this.address_line_3 = address_line_3;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPrimary_mobile_connection_type() {
        return primary_mobile_connection_type;
    }

    public void setPrimary_mobile_connection_type(String primary_mobile_connection_type) {
        this.primary_mobile_connection_type = primary_mobile_connection_type;
    }

    public String getPrimary_mobile_operator() {
        return primary_mobile_operator;
    }

    public void setPrimary_mobile_operator(String primary_mobile_operator) {
        this.primary_mobile_operator = primary_mobile_operator;
    }

    public String getPrimary_mobile_circle() {
        return primary_mobile_circle;
    }

    public void setPrimary_mobile_circle(String primary_mobile_circle) {
        this.primary_mobile_circle = primary_mobile_circle;
    }

    public String getPrimary_mobile_dnc() {
        return primary_mobile_dnc;
    }

    public void setPrimary_mobile_dnc(String primary_mobile_dnc) {
        this.primary_mobile_dnc = primary_mobile_dnc;
    }

    public String getAlternate_mobile_number() {
        return alternate_mobile_number;
    }

    public void setAlternate_mobile_number(String alternate_mobile_number) {
        this.alternate_mobile_number = alternate_mobile_number;
    }

    public String getAlternate_mobile_connection_type() {
        return alternate_mobile_connection_type;
    }

    public void setAlternate_mobile_connection_type(String alternate_mobile_connection_type) {
        this.alternate_mobile_connection_type = alternate_mobile_connection_type;
    }

    public String getAlternate_mobile_operator() {
        return alternate_mobile_operator;
    }

    public void setAlternate_mobile_operator(String alternate_mobile_operator) {
        this.alternate_mobile_operator = alternate_mobile_operator;
    }

    public String getAlternate_mobile_circle() {
        return alternate_mobile_circle;
    }

    public void setAlternate_mobile_circle(String alternate_mobile_circle) {
        this.alternate_mobile_circle = alternate_mobile_circle;
    }

    public String getAlternate_mobile_dnc() {
        return alternate_mobile_dnc;
    }

    public void setAlternate_mobile_dnc(String alternate_mobile_dnc) {
        this.alternate_mobile_dnc = alternate_mobile_dnc;
    }

    public String getRecharge_number() {
        return recharge_number;
    }

    public void setRecharge_number(String recharge_number) {
        this.recharge_number = recharge_number;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String level_id) {
        this.level_id = level_id;
    }

    public String getId_proof() {
        return id_proof;
    }

    public void setId_proof(String id_proof) {
        this.id_proof = id_proof;
    }

    public String getPrimary_verify() {
        return primary_verify;
    }

    public void setPrimary_verify(String primary_verify) {
        this.primary_verify = primary_verify;
    }

    public String getAlternate_verify() {
        return alternate_verify;
    }

    public void setAlternate_verify(String alternate_verify) {
        this.alternate_verify = alternate_verify;
    }

    public String getUser_total_balance() {
        return user_total_balance;
    }

    public void setUser_total_balance(String user_total_balance) {
        this.user_total_balance = user_total_balance;
    }

    public String getUser_redeem_balance() {
        return user_redeem_balance;
    }

    public void setUser_redeem_balance(String user_redeem_balance) {
        this.user_redeem_balance = user_redeem_balance;
    }

    public String getUser_balance() {
        return user_balance;
    }

    public void setUser_balance(String user_balance) {
        this.user_balance = user_balance;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    @Override
    public String toString() {
        return "ElectricianDetailModel{" +
                "id='" + id + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", profession='" + profession + '\'' +
                ", professional_qualification='" + professional_qualification + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", address_line_1='" + address_line_1 + '\'' +
                ", address_line_2='" + address_line_2 + '\'' +
                ", address_line_3='" + address_line_3 + '\'' +
                ", pincode='" + pincode + '\'' +
                ", primary_mobile_connection_type='" + primary_mobile_connection_type + '\'' +
                ", primary_mobile_operator='" + primary_mobile_operator + '\'' +
                ", primary_mobile_circle='" + primary_mobile_circle + '\'' +
                ", primary_mobile_dnc='" + primary_mobile_dnc + '\'' +
                ", alternate_mobile_number='" + alternate_mobile_number + '\'' +
                ", alternate_mobile_connection_type='" + alternate_mobile_connection_type + '\'' +
                ", alternate_mobile_operator='" + alternate_mobile_operator + '\'' +
                ", alternate_mobile_circle='" + alternate_mobile_circle + '\'' +
                ", alternate_mobile_dnc='" + alternate_mobile_dnc + '\'' +
                ", recharge_number='" + recharge_number + '\'' +
                ", created_by='" + created_by + '\'' +
                ", updated_by='" + updated_by + '\'' +
                ", level_id='" + level_id + '\'' +
                ", otherCity='" + otherCity + '\'' +
                ", id_proof='" + id_proof + '\'' +
                ", primary_verify='" + primary_verify + '\'' +
                ", alternate_verify='" + alternate_verify + '\'' +
                ", user_total_balance='" + user_total_balance + '\'' +
                ", user_redeem_balance='" + user_redeem_balance + '\'' +
                ", user_balance='" + user_balance + '\'' +
                ", start_date='" + start_date + '\'' +
                '}';
    }
}
