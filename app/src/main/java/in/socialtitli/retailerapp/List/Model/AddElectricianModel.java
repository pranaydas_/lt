package in.socialtitli.retailerapp.List.Model;

import com.google.gson.annotations.SerializedName;

public class AddElectricianModel {

    @SerializedName("isoid")
    private String isoid;

    @SerializedName("retailerid")
    private String retailerid;

    @SerializedName("username")
    private String username;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("stateid")
    private int state;

    @SerializedName("cityid")
    private int city;

    @SerializedName("pincode")
    private String pincode;

    public AddElectricianModel() {
    }

    public AddElectricianModel(String isoid, String retailerid, String username, String firstName, String lastName, int state, int city, String pincode) {
        this.isoid = isoid;
        this.retailerid = retailerid;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.state = state;
        this.city = city;
        this.pincode = pincode;
    }

    public String getIsoid() {
        return isoid;
    }

    public void setIsoid(String isoid) {
        this.isoid = isoid;
    }

    public String getRetailerid() {
        return retailerid;
    }

    public void setRetailerid(String retailerid) {
        this.retailerid = retailerid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @Override
    public String toString() {
        return "AddElectricianModel{" +
                "isoid='" + isoid + '\'' +
                ", retailerid='" + retailerid + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", state=" + state +
                ", city=" + city +
                ", pincode='" + pincode + '\'' +
                '}';
    }
}
