package in.socialtitli.retailerapp.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;

import in.socialtitli.retailerapp.R;


public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {
    private String latestVersion;
    private String currentVersion;
    private Context context;

    public ForceUpdateAsync(String currentVersion, Context context) {
        this.currentVersion = currentVersion;
        this.context = context;
    }

    @Override
    protected JSONObject doInBackground(String... params) {

        try {
            latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                    .first()
                    .ownText();

            Log.d("check latest version", latestVersion);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        if (latestVersion != null) {
            if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                Log.d("check condition", "....");

                showForceUpdateDialog();
                Log.d("check method", "....");
                // Toast.makeText(context,"update is available.",Toast.LENGTH_LONG).show();
               /* if (!(context instanceof Navigation)) {
                    if (!((Activity) context).isFinishing()) {
                        showForceUpdateDialog();

                        Log.d("check method", "....");
                    }
                }*/
            }
        }
        super.onPostExecute(jsonObject);
    }

    private void showForceUpdateDialog() {

        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title(R.string.youAreNotUpdatedTitle)
                .content(context.getString(R.string.youAreNotUpdatedMessage) + " " + latestVersion + " " + context.getString(R.string.youAreNotUpdatedMessage1))
                .positiveText(R.string.update)
                .autoDismiss(false)
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName()));

                        if (intent.resolveActivity(context.getPackageManager()) != null) {
                            context.startActivity(intent);
                            dialog.dismiss();

                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_play_store_found), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).build();
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                } else {
                    return false;
                }
            }
        });
        dialog.show();
    }
}
