package in.socialtitli.retailerapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import in.socialtitli.retailerapp.Base.App;
import in.socialtitli.retailerapp.Model.ParentDetailsModel;
import in.socialtitli.retailerapp.Model.UserModel;

public class PreferenceUtil {
    private static SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getContext());

    private static UserModel user = null;
    private static ParentDetailsModel parent = null;
    private static final String USER_MULTILINGUAL = "user_multilingual";
    public static String LANGUAGE_ENG = "en";
    public static String LANGUAGE_MAR = "mr";
    public static String LANGUAGE_HIN = "hi";

    public static boolean isUserLoggedIn() {
        return preferences.getBoolean("is_logged_in", false);
    }

    public static void setUserLoggedIn(boolean isLoggedIn) {
        preferences.edit().putBoolean("is_logged_in", isLoggedIn).apply();
    }

    public static UserModel getUser() {
        if (user == null) {
            user = new Gson().fromJson(preferences.getString("user", null), UserModel.class);
        }
        return user;
    }

    public static void clearUserData() {
        if (user != null) {
            user = null;
        }
    }

    public static void setUser(UserModel user) {
        preferences.edit().putString("user", new Gson().toJson(user)).apply();
    }

    public static void setParent(ParentDetailsModel parent) {
        preferences.edit().putString("parent", new Gson().toJson(parent)).apply();
    }

    public static ParentDetailsModel getParent() {
        if (parent == null) {
            parent = new Gson().fromJson(preferences.getString("parent", null), ParentDetailsModel.class);
        }
        return parent;
    }

    public static void clearAll() {
        user = null;
        preferences.edit().clear().apply();
    }

    public static void clear() {
        user = null;
    }

    public static String getFilterType() {
        return preferences.getString("filterType", "loadData");
    }

    public static void setFilterType(String filterType) {
        preferences.edit().putString("filterType", filterType).apply();
    }

    public static String getEventId() {
        return preferences.getString("eventId", null);
    }

    public static void setEventId(String eventId) {
        preferences.edit().putString("eventId", eventId).apply();
    }

    // save language
    public static void putString(Context activity, String key) {
        preferences.edit().putString(USER_MULTILINGUAL, key).apply();
    }

    public static String getString() {
        return preferences.getString(USER_MULTILINGUAL, null);
    }
}
