package in.socialtitli.retailerapp.Utils;

public class ResponseCodes {
    public static final int SUCCESS = 200;
    public static final int MOBILE_NUMBER_NOT_REGISTERED = 403;
    public static final int BAD_REQUEST = 400;
    public static final int CONFLICT = 409;
    public static final int ACCEPTED = 202;
    public static final int NOT_FOUND = 404;
    public static final int UNAUTHORIZED = 401;
    public static final int NOT_ACCEPTABLE = 406;
    public static final int INTERNAL_SERVER = 500;
    public static final int END_OF_LIST = 212;

    public static final int GIFT_NOT_AVAILABLE = 213;
    public static final int SOMETHING_WENT_WRONG = 214;
    public static final int NOT_ENOUGH_POINTS = 215;
    public static final int OTP_SENT = 220;

    public static final String GIFT_NOT_AVAILABLE_STRING = "213";
    public static final String SOMETHING_WENT_WRONG_STRING = "214";
    public static final String NOT_ENOUGH_POINTS_STRING = "215";
    public static final String OTP_SENT_STRING = "220";
}
