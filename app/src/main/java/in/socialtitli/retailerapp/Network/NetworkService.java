package in.socialtitli.retailerapp.Network;

import java.util.List;

import in.socialtitli.retailerapp.Bill.Activity.BillUpload;
import in.socialtitli.retailerapp.Bill.Model.BillModel;
import in.socialtitli.retailerapp.Bill.Model.BillUploadModel;
import in.socialtitli.retailerapp.Bill.Model.DailyPointsReqModel;
import in.socialtitli.retailerapp.ContactAdmin.Model.FeedbackModel;
import in.socialtitli.retailerapp.CounterBoy.Model.CounterBoyModel;
import in.socialtitli.retailerapp.Dashboard.Model.DailyPointsModel;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.Dashboard.Model.PointsDateModel;
import in.socialtitli.retailerapp.Dashboard.Model.RetailerPointsModel;
import in.socialtitli.retailerapp.Dashboard.Model.SchemeModel;
import in.socialtitli.retailerapp.Dashboard.Model.SchemePointsModel;
import in.socialtitli.retailerapp.Gifts.Model.CheckGiftModel;
import in.socialtitli.retailerapp.Gifts.Model.GiftResponseModel;
import in.socialtitli.retailerapp.Gifts.Model.GiftsModel;
import in.socialtitli.retailerapp.List.Model.AddElectricianModel;
import in.socialtitli.retailerapp.List.Model.ElectricianDetailModel;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.List.Model.PromoCodeHistoryModel;
import in.socialtitli.retailerapp.Model.CityList;
import in.socialtitli.retailerapp.Model.ParentDetailsModel;
import in.socialtitli.retailerapp.Model.StateList;
import in.socialtitli.retailerapp.Model.UserModel;
import in.socialtitli.retailerapp.MyProfile.Model.RetailerUpdateModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface NetworkService {

    @POST("login.php")
    Call<String> loginUser(@Header("Username") String userName);

    @POST("validate_otp.php")
    Call<UserModel> verifyOtp(@Header("Username") String userName, @Header("Otp") String otp);

    @POST("add_feedback.php")
    Call<String> sendFeedback(@Body FeedbackModel feedbackModel);

    @POST("electrician_list.php")
    Call<List<ElectricianListModel>> fetchingElectricianList(@Header("Retailerid") String retailerId);

    @POST("parent_id.php")
    Call<ParentDetailsModel> fetchParent(@Header("Userid") String retailerId);

    @POST("state.php")
    Call<List<StateList>> fetchStates();

    @POST("city.php")
    Call<List<CityList>> fetchCities(@Header("Stateid") int stateId);

    @POST("add_electrician.php")
    Call<String> addElectrician(@Body AddElectricianModel addElectricianModel);

    @POST("gift.php")
    Call<List<GiftsModel>> fetchingGifts();

    @POST("retailer_points.php")
    Call<RetailerPointsModel> fetchPoints(@Header("Retailerid") String retailerId);

//    @POST("retailer_passbook.php")
//    Call<List<PassbookModel>> fetchElectricianPoints(@Header("Retailerid") String retailerId);

    @POST("electrician_points_date.php")
    Call<List<PointsDateModel>> fetchElectricianPoints(@Header("Retailerid") String retailerId);

    @POST("daily_ele_points.php")
    Call<List<DailyPointsModel>> fetchDailyPoints(@Body DailyPointsReqModel dailyPointsReqModel);

    @POST("scheme_points.php")
    Call<List<SchemePointsModel>> fetchSchemePoints(@Header("Retailerid") String retailerId);

    @POST("electrician_details.php")
    Call<ElectricianDetailModel> fetchingElectricianDetail(@Header("Retailerid") String retailerId, @Header("Electricianid") String electricianId);

    @POST("electrician_points_history_api.php")
    Call<List<PromoCodeHistoryModel>> fetchingPromoCodeHistory(@Header("Retailerid") String retailerId, @Header("Electricianid") String electricianId);

    @POST("order_history.php")
    Call<List<OrderHistoryModel>> fetchOrderHistory(@Header("Retailerid") String retailerId);

    @POST("scheme_offer.php")
    Call<List<SchemeModel>> fetchSchemes();

    @POST("redeem_gift.php")
    Call<GiftResponseModel> redeemGift(@Body CheckGiftModel checkGiftModel);

    @POST("verify_gift.php")
    Call<String> confirmBuyGift(@Header("Username") String retailerId, @Header("Otp") String otp, @Body CheckGiftModel checkGiftModel);

    @POST("update_retailer.php")
    Call<String> updateRetailer(@Body RetailerUpdateModel retailerUpdateModel);

    @POST("fetch_retailer_bills.php")
    Call<List<BillModel>> fetchBills(@Header("Retailerid") String retailerId, @Header("Mobnumber") String mobNumber);

    @POST("upload_retailer_bill.php")
    Call<String> uploadBill(@Body BillUploadModel billUpload);

    @POST("retailer_detailes.php")
    Call<UserModel> getUserProfile(@Header("Username") String mobNumber);

    @POST("order_details.php")
    Call<OrderHistoryModel> getOrderDetails(@Header("Retailerid") String retailerId, @Header("Orderid") String orderId);

    @POST("add_counter_boy.php")
    Call<String> addCounterBoy(@Body CounterBoyModel counterBoyModel);

    @POST("fetch_counter_boy.php")
    Call<List<CounterBoyModel>> fetchCounterBoy(@Header("Retailerid") String retailerId);

    @POST("fetch_counter_boy_details.php")
    Call<CounterBoyModel> fetchCounterBoyDetails(@Header("Retailerid") String retailerId, @Header("Counterboyid") String counterBoyId);

    @POST("edit_counter_boy.php")
    Call<String> editCounterBoy(@Header("Counterboyid") String counterBoyId, @Body CounterBoyModel counterBoyModel);
}