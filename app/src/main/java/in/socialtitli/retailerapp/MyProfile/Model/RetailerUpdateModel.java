package in.socialtitli.retailerapp.MyProfile.Model;

import com.google.gson.annotations.SerializedName;

public class RetailerUpdateModel {

    @SerializedName("name")
    private String name;

    @SerializedName("date_of_birth")
    private String dateOfBirth;

    @SerializedName("mobile_no")
    private String mobile_no;

    @SerializedName("state")
    private int stateid;

    @SerializedName("city")
    private int cityid;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("address_1")
    private String addressLine1;

    @SerializedName("address_2")
    private String addressLine2;

    @SerializedName("address_3")
    private String addressLine3;

    @SerializedName("gst_no")
    private String gstNumber;

    @SerializedName("image")
    private String image;

    @SerializedName("base64")
    private String base64;

    @SerializedName("retailerid")
    private String id;

    @SerializedName("tmp_address")
    private String tmp_address;

    public RetailerUpdateModel(){}

    public RetailerUpdateModel(String name, String dateOfBirth, String mobile_no, int stateid, int cityid, String pincode, String addressLine1, String addressLine2, String addressLine3, String gstNumber, String image, String base64, String id, String tmp_address) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.mobile_no = mobile_no;
        this.stateid = stateid;
        this.cityid = cityid;
        this.pincode = pincode;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.gstNumber = gstNumber;
        this.image = image;
        this.base64 = base64;
        this.id = id;
        this.tmp_address = tmp_address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public int getStateid() {
        return stateid;
    }

    public void setStateid(int stateid) {
        this.stateid = stateid;
    }

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTmp_address() {
        return tmp_address;
    }

    public void setTmp_address(String tmp_address) {
        this.tmp_address = tmp_address;
    }

    @Override
    public String toString() {
        return "RetailerUpdateModel{" +
                "name='" + name + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", mobile_no='" + mobile_no + '\'' +
                ", stateid=" + stateid +
                ", cityid=" + cityid +
                ", pincode='" + pincode + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", addressLine3='" + addressLine3 + '\'' +
                ", gstNumber='" + gstNumber + '\'' +
                ", image='" + image + '\'' +
                ", base64='" + base64 + '\'' +
                ", id='" + id + '\'' +
                ", tmp_address='" + tmp_address + '\'' +
                '}';
    }
}
