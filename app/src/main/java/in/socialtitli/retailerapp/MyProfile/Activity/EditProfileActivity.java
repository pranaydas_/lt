package in.socialtitli.retailerapp.MyProfile.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Model.SchemeModel;
import in.socialtitli.retailerapp.Login.LoginActivity;
import in.socialtitli.retailerapp.Model.CityList;
import in.socialtitli.retailerapp.Model.StateList;
import in.socialtitli.retailerapp.Model.UserModel;
import in.socialtitli.retailerapp.MyProfile.Model.RetailerUpdateModel;
import in.socialtitli.retailerapp.MyProfile.ProfileFragment;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityEditProfileBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityEditProfileBinding binding;
    private String retailerIdStr, mobStr;
    private UserModel userModel;
    private String nameStr, address1Str, address2Str, address3Str, gstStr, pincodeStr, cityStr,
            dobStr, stateStr, tempAddressStr, oldImageStr, encodedBase64, fileName, today, cityOldStr="";
    private int statePos, cityPos, selId, cityIdSpinner;
    private List<String> states, cities;
    private List<Integer> statesIdDb, citiesIdDb;
    ArrayAdapter stateAdapter, cityAdapter;
    private int REQUEST_CAMERA = 1, SELECT_FILE = 0, year, month, day;
    private static int MY_PERMISSIONS_REQUEST_CAMERA = 11;
    private static int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 12;
    private List<StateList> stateList = new ArrayList<>();
    private List<CityList> cityList = new ArrayList<>();
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        mCon = this;

        if (PreferenceUtil.getUser() != null) {
            retailerIdStr = PreferenceUtil.getUser().getId();
            mobStr = PreferenceUtil.getUser().getMobileNumber();
        } else {
            new Intent(mCon, LoginActivity.class);
            finish();
        }

        setUserDetails(mobStr);

        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        today = "" + day + "-" + (month + 1) + "-" + year;

        binding.dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(mCon, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            binding.dob.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                        }
                    }, year, month, day);
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
                    datePickerDialog.show();
                }
            }
        });

        binding.profilePicSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mCon,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

                } else if (ContextCompat.checkSelfPermission(mCon,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

                } else {
                    galleryIntent();
                }
            }
        });

        binding.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameStr = binding.nameEditText.getText().toString().trim();
                dobStr = binding.dob.getText().toString();
                gstStr = binding.gstEditText.getText().toString().trim();
                address1Str = binding.address1EditText.getText().toString().trim();
                address2Str = binding.address2EditText.getText().toString().trim();
                address3Str = binding.address3EditText.getText().toString().trim();
                tempAddressStr = binding.tempAddressEditText.getText().toString().trim();
                pincodeStr = binding.pincodeEditText.getText().toString().trim();

                validate();
            }
        });
    }

    private void setUserDetails(String mobNo) {
        try {
            Call<UserModel> call = ApiClient.getNetworkService().getUserProfile(mobNo);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .content("Fetching Profile")
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    userModel = response.body();

                    if (userModel.getName() != null){
                        binding.nameEditText.setText(userModel.getName());
                    }
                    if (userModel.getDateOfBirth() != null){
                        binding.dob.setText(userModel.getDateOfBirth());
                    }
                    if (userModel.getGstNumber() != null){
                        binding.gstEditText.setText(userModel.getGstNumber());
                    }
                    if (userModel.getAddressLine1() != null){
                        binding.address1EditText.setText(userModel.getAddressLine1());
                    }
                    if (userModel.getAddressLine2() != null){
                        binding.address2EditText.setText(userModel.getAddressLine2());
                    }
                    if (userModel.getAddressLine3() != null){
                        binding.address3EditText.setText(userModel.getAddressLine3());
                    }
                    if (userModel.getTmp_address() != null){
                        binding.tempAddressEditText.setText(userModel.getTmp_address());
                    }
                    if (userModel.getPincode() != null) {
                        binding.pincodeEditText.setText(userModel.getPincode());
                    }
                    if (userModel.getCity() != null && !(userModel.getCity().equalsIgnoreCase(""))) {
                        cityOldStr = userModel.getCity();
                    }
                    if (userModel.getStateid() != null && !(userModel.getStateid().equalsIgnoreCase(""))) {
                        loadState(Integer.parseInt(userModel.getStateid()));
                    } else {
                        loadStateNew();
                        binding.stateSpinner.setSelection(0);
                    }

                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }

    private void loadStateNew() {
        try {
            Call<List<StateList>> call = ApiClient.getNetworkService().fetchStates();

            call.enqueue(new Callback<List<StateList>>() {
                @Override
                public void onResponse(Call<List<StateList>> call, Response<List<StateList>> response) {
                    if (response.isSuccessful()) {
                        stateList = response.body();

                        if (stateList != null) {

                            states = new ArrayList<>();
                            statesIdDb = new ArrayList<>();
                            states.add("Select State");
                            statesIdDb.add(0);

                            for (StateList model : stateList) {
                                states.add(model.getStateName());
                                statesIdDb.add(model.getStateId());
                            }

                            stateAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, states);
                            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.stateSpinner.setAdapter(stateAdapter);

                            binding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    stateStr = binding.stateSpinner.getSelectedItem().toString();
                                    statePos = binding.stateSpinner.getSelectedItemPosition();

                                    selId = statesIdDb.get(statePos);

                                    loadCities(selId);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<StateList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadState(int stateId) {
        try {
            Call<List<StateList>> call = ApiClient.getNetworkService().fetchStates();

            call.enqueue(new Callback<List<StateList>>() {
                @Override
                public void onResponse(Call<List<StateList>> call, Response<List<StateList>> response) {
                    if (response.isSuccessful()) {
                        stateList = response.body();

                        if (stateList != null) {

                            states = new ArrayList<>();
                            statesIdDb = new ArrayList<>();
                            states.add("Select State");
                            statesIdDb.add(0);

                            for (StateList model : stateList) {
                                states.add(model.getStateName());
                                statesIdDb.add(model.getStateId());
                            }

                            stateAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, states);
                            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.stateSpinner.setAdapter(stateAdapter);

                            for (int k = 0; k <= statesIdDb.size()-1; k++) {
                                if (statesIdDb.get(k) == stateId)
                                {
                                    binding.stateSpinner.setSelection(k);
                                }
                            }

                            binding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    stateStr = binding.stateSpinner.getSelectedItem().toString();
                                    statePos = binding.stateSpinner.getSelectedItemPosition();

                                    selId = statesIdDb.get(statePos);

                                    loadCities(selId);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<StateList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCities(int stateId) {
        try {
            Call<List<CityList>> call = ApiClient.getNetworkService().fetchCities(stateId);

            call.enqueue(new Callback<List<CityList>>() {
                @Override
                public void onResponse(Call<List<CityList>> call, Response<List<CityList>> response) {
                    if (response.isSuccessful()) {
                        cityList = response.body();

                        if (cityList != null) {

                            cities = new ArrayList<>();
                            citiesIdDb = new ArrayList<>();
                            cities.add("Select City");
                            citiesIdDb.add(0);

                            for (CityList model : cityList) {
                                cities.add(model.getCityName());
                                citiesIdDb.add(model.getCityId());
                            }

                            cityAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, cities);
                            cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.citySpinner.setAdapter(cityAdapter);

                            if (!cityOldStr.equalsIgnoreCase("")){
                                String compareValue = cityOldStr;
                                if (compareValue != null) {
                                    int spinnerPosition = cityAdapter.getPosition(compareValue);
                                    binding.citySpinner.setSelection(spinnerPosition);
                                }
                            }

                            binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    cityStr = binding.citySpinner.getSelectedItem().toString();
                                    cityPos = binding.citySpinner.getSelectedItemPosition();

                                    cityIdSpinner = citiesIdDb.get(cityPos);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    }
                }

                @Override
                public void onFailure(Call<List<CityList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryIntent() {
        String[] mimeTypes = {"image/jpeg", "image/png", "image/jpg"};
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), SELECT_FILE);
    }

    private void validate() {
        boolean isValidName = false, isValidGst = false, isValidAddress1 = false, isValidPincode = false, isValidCity = false,
                isValidDob = false;

        //Name
        if (TextUtils.isEmpty(nameStr)) {
            binding.nameTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.nameTextLayout.setError(null);
            isValidName = true;
        }

        //Dob
        if (TextUtils.isEmpty(dobStr)) {
            binding.nameTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.nameTextLayout.setError(null);
            isValidDob = true;
        }

        //GST
        if (gstStr.length() > 0) {
            if (gstStr.length() < 15) {
                binding.gstTextLayout.setError(getResources().getString(R.string.enter_valid_gst));
            } else {
                isValidGst = true;
                binding.gstTextLayout.setError(null);
            }
        } else {
            binding.gstTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Address
        if (TextUtils.isEmpty(address1Str)) {
            binding.address1TextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.address1TextLayout.setError(null);
            isValidAddress1 = true;
        }

        //Pincode
        if (pincodeStr.length() > 0) {
            if (pincodeStr.length() < 6) {
                binding.pincodeTextLayout.setError(getResources().getString(R.string.enter_valid_pincode));
            } else {
                isValidPincode = true;
                binding.pincodeTextLayout.setError(null);
            }
        } else {
            binding.pincodeTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //City
        if (cityPos != 0)
        {
            isValidCity = true;
        } else {
            Toast.makeText(mCon, "Select Valid City", Toast.LENGTH_SHORT).show();
        }

        if (isValidName && isValidDob && isValidGst && isValidAddress1 && isValidPincode && isValidCity) {

            RetailerUpdateModel retailerUpdateModel = new RetailerUpdateModel(nameStr, dobStr, mobStr, selId, cityIdSpinner, pincodeStr,
                    address1Str, address2Str, address3Str, gstStr, fileName, encodedBase64, retailerIdStr, tempAddressStr);
            updateRetailer(retailerUpdateModel);
        }
    }

    private void updateRetailer(RetailerUpdateModel retailerUpdateModel) {
        try {
            Call<String> call = ApiClient.getNetworkService().updateRetailer(retailerUpdateModel);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.updating_profile)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {
                            Toast.makeText(mCon, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                            finish();
                            dialog.dismiss();
                        }
                        else {
                            Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (reqCode == SELECT_FILE) {
                Cursor returnCursor = null;

                try {
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        ContentResolver cr = mCon.getContentResolver();
                        String mime = cr.getType(imageUri);

                        returnCursor = mCon.getContentResolver().query(imageUri, null, null, null, null);
                        if (returnCursor != null) {

                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();

                            if (mime != null && mime.equals("image/jpeg") || mime != null && mime.equals("image/jpg") || mime != null && mime.equals("image/png")) {

                                if (returnCursor.getLong(sizeIndex) > 2000000) {
                                    Toast.makeText(mCon, "Image size is too large", Toast.LENGTH_SHORT).show();

                                } else {

                                    final InputStream imageStream = mCon.getContentResolver().openInputStream(imageUri);
                                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));

                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }

                                    File insideFolder = new File(dir, getResources().getString(R.string.documents));

                                    if (!insideFolder.exists()) {
                                        insideFolder.mkdirs();
                                    }

                                    File destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");

                                    String strPath = String.valueOf(destination);

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    if (!strPath.equalsIgnoreCase("")) {

                                        if (bytes.size() < 2000000) {

                                            byte[] imageBytes = bytes.toByteArray();
                                            encodedBase64 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                                            fileName = strPath.substring(strPath.lastIndexOf('/') + 1);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mCon, "Document Type Should be jpeg , jpg , png .", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //   Toast.makeText(mCon, R.string.someThingWentWrong, Toast.LENGTH_SHORT).show();
                } finally {
                    if (returnCursor != null) {
                        returnCursor.close();
                    }
                }

            }

        }
    }
}