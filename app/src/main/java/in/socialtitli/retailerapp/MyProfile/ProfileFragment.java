package in.socialtitli.retailerapp.MyProfile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import in.socialtitli.retailerapp.Login.LoginActivity;
import in.socialtitli.retailerapp.Login.OtpScreenActivity;
import in.socialtitli.retailerapp.MainActivity;
import in.socialtitli.retailerapp.Model.UserModel;
import in.socialtitli.retailerapp.MyProfile.Activity.EditProfileActivity;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.FragmentProfileBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private Context mCon;
    UserModel model;
    private String firstNameStr, lastNameStr, userNameStr, emailStr, mobileNoStr, stateStr, cityStr, gstStr, add1Str, add2Str, add3Str,
                    dobStr, pincodeStr, mobNo;
    private static final int REQUEST_CODE = 20;
    private MaterialDialog dialog;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mCon = getActivity();
        View view = binding.getRoot();

        if (PreferenceUtil.getUser() != null) {
            mobNo = PreferenceUtil.getUser().getMobileNumber();
            getUserProfile(mobNo);
        }

        binding.editProfileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mCon, EditProfileActivity.class));
            }
        });

        return view;
    }

    private void getUserProfile(String mobNo) {
        try {
            Call<UserModel> call = ApiClient.getNetworkService().getUserProfile(mobNo);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .content("Fetching Profile")
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    handleResponseCode(response);
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }

    private void handleResponseCode(Response<UserModel> response) {
        switch (response.code()) {
            case ResponseCodes.MOBILE_NUMBER_NOT_REGISTERED:
                Toast.makeText(mCon, "Invalid User", Toast.LENGTH_SHORT).show();
                break;

            case ResponseCodes.SUCCESS:
                model = response.body();

                if (model != null)
                {
                    if (model.getImage() != null && !(model.getImage().equalsIgnoreCase(""))){
                        Glide
                                .with(mCon)
                                .load(model.getImage())
                                .centerCrop()
                                .into(binding.imageCardview);
                    }

                    if (model.getName() != null && !(model.getName().equalsIgnoreCase(""))){
                        binding.nameTextView.setText(model.getName());
                        binding.userNameTextView.setText(model.getName());
                    }

                    if (model.getMobileNumber() != null && !(model.getMobileNumber().equalsIgnoreCase(""))){
                        binding.mobileNoTextView.setText(model.getMobileNumber());
                    }

                    if (model.getDateOfBirth() != null && !(model.getDateOfBirth().equalsIgnoreCase(""))){
                        binding.dobTextView.setText(model.getDateOfBirth());
                    } else {
                        binding.dobTextView.setText("NA");
                    }

                    if (model.getGstNumber() != null && !(model.getGstNumber().equalsIgnoreCase(""))){
                        binding.gstTextView.setText(model.getGstNumber());
                    } else {
                        binding.gstTextView.setText("NA");
                    }

                    if (model.getAddressLine1() != null && !(model.getAddressLine1().equalsIgnoreCase(""))){
                        binding.addressLine1TextView.setText(model.getAddressLine1());
                    } else {
                        binding.addressLine1TextView.setText("NA");
                    }

                    if (model.getAddressLine2() != null && !(model.getAddressLine2().equalsIgnoreCase(""))){
                        binding.addressLine2TextView.setText(model.getAddressLine2());
                    } else {
                        binding.addressLine2TextView.setText("NA");
                    }

                    if (model.getAddressLine3() != null && !(model.getAddressLine3().equalsIgnoreCase(""))){
                        binding.addressLine3TextView.setText(model.getAddressLine3());
                    } else {
                        binding.addressLine3TextView.setText("NA");
                    }

                    if (model.getTmp_address() != null && !(model.getTmp_address().equalsIgnoreCase(""))){
                        binding.tempAddressTextView.setText(model.getTmp_address());
                    } else {
                        binding.tempAddressTextView.setText("NA");
                    }

                    if (model.getState() != null && !(model.getState().equalsIgnoreCase(""))){
                        binding.stateTextView.setText(model.getState());
                    } else {
                        binding.stateTextView.setText("NA");
                    }

                    if (model.getCity() != null && !(model.getCity().equalsIgnoreCase(""))){
                        binding.cityTextView.setText(model.getCity());
                    } else {
                        binding.cityTextView.setText("NA");
                    }

                    if (model.getPincode() != null && !(model.getPincode().equalsIgnoreCase(""))) {
                        binding.pincodeTextView.setText(model.getPincode());
                    } else {
                        binding.pincodeTextView.setText("NA");
                    }
                }

                break;
        }
    }

}