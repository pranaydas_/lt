package in.socialtitli.retailerapp.Bill.Model;

import com.google.gson.annotations.SerializedName;

public class BillUploadModel {
    @SerializedName("retailerid")
    private String retailerid;

    @SerializedName("schemeid")
    private String schemeid;

    @SerializedName("mobileno")
    private String mobileno;

    @SerializedName("base64")
    private String base64;

    @SerializedName("image")
    private String image;

    public BillUploadModel(){}

    public BillUploadModel(String retailerid, String schemeid, String mobileno, String base64, String image) {
        this.retailerid = retailerid;
        this.schemeid = schemeid;
        this.mobileno = mobileno;
        this.base64 = base64;
        this.image = image;
    }

    public String getRetailerid() {
        return retailerid;
    }

    public void setRetailerid(String retailerid) {
        this.retailerid = retailerid;
    }

    public String getSchemeid() {
        return schemeid;
    }

    public void setSchemeid(String schemeid) {
        this.schemeid = schemeid;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "BillUploadModel{" +
                "retailerid='" + retailerid + '\'' +
                ", schemeid='" + schemeid + '\'' +
                ", mobileno='" + mobileno + '\'' +
                ", base64='" + base64 + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
