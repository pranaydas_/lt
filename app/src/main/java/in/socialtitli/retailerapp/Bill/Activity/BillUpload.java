package in.socialtitli.retailerapp.Bill.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import in.socialtitli.retailerapp.Bill.Model.BillUploadModel;
import in.socialtitli.retailerapp.Dashboard.Model.SchemeModel;
import in.socialtitli.retailerapp.Model.StateList;
import in.socialtitli.retailerapp.MyProfile.ProfileFragment;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityBillUploadBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillUpload extends AppCompatActivity {

    private ActivityBillUploadBinding binding;
    private Context mCon;
    private BottomSheetDialog selectTypeDialog;
    private RelativeLayout takePhoto, selectFromGallery;
    private File destination;
    private int REQUEST_CAMERA = 1, SELECT_FILE = 0, SELECT_PDF = 2;

    private static int MY_PERMISSIONS_REQUEST_CAMERA = 11;
    private static int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 12;
    private String retailerIdStr, encodedBase64, fileName, mobStr, schemeStr, selId;
    private MaterialDialog dialog;
    ArrayAdapter schemeAdapter;
    private List<SchemeModel> schemeModelsList;
    private List<String> schemes, schemeId;
    private int schemePos;

    //Uri to store the image uri
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bill_upload);
        mCon = this;

        retailerIdStr = PreferenceUtil.getUser().getId();
        mobStr = PreferenceUtil.getUser().getMobileNumber();

        loadSchemes();

        binding.buttonChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mCon,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

                } else if (ContextCompat.checkSelfPermission(mCon,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) mCon, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);

                } else {
                    galleryIntent();
                }
            }
        });

        binding.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BillUploadModel billUploadmodel = new BillUploadModel(retailerIdStr, selId, mobStr, encodedBase64, fileName);
                uploadBill(billUploadmodel);
            }
        });
    }

    private void loadSchemes() {
        try {
            Call<List<SchemeModel>> call = ApiClient.getNetworkService().fetchSchemes();

            call.enqueue(new Callback<List<SchemeModel>>() {
                @Override
                public void onResponse(Call<List<SchemeModel>> call, Response<List<SchemeModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            schemeModelsList = response.body();

                            if (schemeModelsList != null) {

                                schemes = new ArrayList<>();
                                schemes.add("Select Schemes");
                                schemeId = new ArrayList<>();
                                schemeId.add("0");

                                for (SchemeModel model : schemeModelsList) {
                                    schemes.add(model.getTitle());
                                    schemeId.add(model.getId());
                                }

                                schemeAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, schemes);
                                schemeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                binding.schemeSpinner.setAdapter(schemeAdapter);

                                binding.schemeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        schemePos = binding.schemeSpinner.getSelectedItemPosition();
                                        selId = schemeId.get(schemePos);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            } else {
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            }

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<SchemeModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }

    private void uploadBill(BillUploadModel billUploadmodel) {
        try {
            Call<String> call = ApiClient.getNetworkService().uploadBill(billUploadmodel);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title("Uploading Bill")
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {
                            Toast.makeText(mCon, "Bill Uploaded successfully", Toast.LENGTH_SHORT).show();
                            finish();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }

//    private void selectType() {
//        final View sheetView = getLayoutInflater().inflate(R.layout.document_layout, null);
//        selectTypeDialog.setContentView(sheetView);
//        selectTypeDialog.setCanceledOnTouchOutside(true);
//        selectTypeDialog.setCancelable(false);
//
//        selectFromGallery = sheetView.findViewById(R.id.selectPhotoRelativeLayout);
//        takePhoto = sheetView.findViewById(R.id.takephotoRelativeLayout);
//
//        takePhoto.setOnClickListener(v -> {
//            cameraIntent();
//            selectTypeDialog.dismiss();
//        });
//
//
//        selectFromGallery.setOnClickListener(v -> {
//            galleryIntent();
//            selectTypeDialog.dismiss();
//        });
//
//    }

    private void galleryIntent() {
        String[] mimeTypes = {"image/jpeg", "image/png", "image/jpg"};
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Select Picture"), SELECT_FILE);

    }

//    private void cameraIntent() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));
//
//        if (!dir.exists()) {
//            dir.mkdirs();
//        }
//
//        File insideFolder = new File(dir, getResources().getString(R.string.documents));
//
//        if (!insideFolder.exists()) {
//            insideFolder.mkdirs();
//        }
//        destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");
//
//        Uri photoURI = FileProvider.getUriForFile(mCon,
//                BuildConfig.APPLICATION_ID + ".provider",
//                destination);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//
//        startActivityForResult(intent, REQUEST_CAMERA);
//    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (reqCode == SELECT_FILE) {
                Cursor returnCursor = null;

                try {
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        ContentResolver cr = mCon.getContentResolver();
                        String mime = cr.getType(imageUri);

                        returnCursor = mCon.getContentResolver().query(imageUri, null, null, null, null);
                        if (returnCursor != null) {

                            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                            returnCursor.moveToFirst();

                            if (mime != null && mime.equals("image/jpeg") || mime != null && mime.equals("image/jpg") || mime != null && mime.equals("image/png")) {

                                if (returnCursor.getLong(sizeIndex) > 2000000) {
                                    Toast.makeText(mCon, "Image size is too large", Toast.LENGTH_SHORT).show();

                                } else {

                                    final InputStream imageStream = mCon.getContentResolver().openInputStream(imageUri);
                                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                                    File dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.folderName));

                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }

                                    File insideFolder = new File(dir, getResources().getString(R.string.documents));

                                    if (!insideFolder.exists()) {
                                        insideFolder.mkdirs();
                                    }

                                    File destination = new File(insideFolder, System.currentTimeMillis() + ".jpg");

                                    String strPath = String.valueOf(destination);

                                    FileOutputStream fo;
                                    try {
                                        destination.createNewFile();
                                        fo = new FileOutputStream(destination);
                                        fo.write(bytes.toByteArray());
                                        fo.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    if (!strPath.equalsIgnoreCase("")) {

                                        if (bytes.size() < 2000000) {

                                            byte[] imageBytes = bytes.toByteArray();
                                            encodedBase64 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                                            fileName = strPath.substring(strPath.lastIndexOf('/') + 1);
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mCon, "Document Type Should be jpeg , jpg , png .", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //   Toast.makeText(mCon, R.string.someThingWentWrong, Toast.LENGTH_SHORT).show();
                } finally {
                    if (returnCursor != null) {
                        returnCursor.close();
                    }
                }

            }

        }
    }
}