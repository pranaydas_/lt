package in.socialtitli.retailerapp.Bill.Model;

import com.google.gson.annotations.SerializedName;

public class BillModel {
    @SerializedName("id")
    private String id;

    @SerializedName("retailer_id")
    private String retailer_id;

    @SerializedName("scheme_id")
    private String scheme_id;

    @SerializedName("image")
    private String image;

    @SerializedName("title")
    private String title;

    public BillModel(){}

    public BillModel(String id, String retailer_id, String scheme_id, String image, String title) {
        this.id = id;
        this.retailer_id = retailer_id;
        this.scheme_id = scheme_id;
        this.image = image;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRetailer_id() {
        return retailer_id;
    }

    public void setRetailer_id(String retailer_id) {
        this.retailer_id = retailer_id;
    }

    public String getScheme_id() {
        return scheme_id;
    }

    public void setScheme_id(String scheme_id) {
        this.scheme_id = scheme_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "BillModel{" +
                "id='" + id + '\'' +
                ", retailer_id='" + retailer_id + '\'' +
                ", scheme_id='" + scheme_id + '\'' +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
