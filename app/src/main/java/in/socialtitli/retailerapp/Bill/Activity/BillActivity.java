package in.socialtitli.retailerapp.Bill.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.Bill.Adapter.BillAdapter;
import in.socialtitli.retailerapp.Bill.Model.BillModel;
import in.socialtitli.retailerapp.Dashboard.Adapter.OrderHistoryAdapter;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityBillBinding;
import in.socialtitli.retailerapp.databinding.ActivityBillUploadBinding;
import in.socialtitli.retailerapp.databinding.ActivityOrderHistoryBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillActivity extends AppCompatActivity {

    private ActivityBillBinding binding;
    private Context mCon;
    private String retailerIdStr, mobNumStr;
    private List<BillModel> billModel;
    private MaterialDialog dialog;
    private BillAdapter billAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bill);
        mCon = this;

        retailerIdStr = PreferenceUtil.getUser().getId();
        mobNumStr = PreferenceUtil.getUser().getMobileNumber();
        Objects.requireNonNull(getSupportActionBar()).setTitle("Bills");

        billAdapter = new BillAdapter(mCon, retailerIdStr);

        binding.billRecyclerView.setHasFixedSize(true);
        binding.billRecyclerView.setLayoutManager(new LinearLayoutManager(mCon));
        binding.billRecyclerView.setItemAnimator(new DefaultItemAnimator());

        loadBills();

        binding.uploadBillFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mCon, BillUpload.class));
            }
        });
    }

    private void loadBills() {
        try {
            Call<List<BillModel>> call = ApiClient.getNetworkService().fetchBills(retailerIdStr, mobNumStr);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title("Fetching Bills")
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<List<BillModel>>() {
                @Override
                public void onResponse(Call<List<BillModel>> call, Response<List<BillModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            if (response.body() != null) {

                                billModel = response.body();


                                binding.errorLinear.setVisibility(View.GONE);
                                binding.billRecyclerView.setVisibility(View.VISIBLE);
                                billAdapter.addAll(billModel);
                                binding.billRecyclerView.setAdapter(billAdapter);
                                billAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            } else {
                                binding.billRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.data_not_found);
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        /*dialogDisplay = false;
                        pageIndex = pageIndex + 10000;
                        loadElectricianData();*/

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            try {
                                binding.billRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.no_data);
                                dialog.dismiss();
                                //Toast.makeText(mCon, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            binding.billRecyclerView.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(R.string.data_not_found);
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        binding.billRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<BillModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        binding.billRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }
}