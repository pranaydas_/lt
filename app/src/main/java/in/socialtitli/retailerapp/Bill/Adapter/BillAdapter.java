package in.socialtitli.retailerapp.Bill.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Bill.Model.BillModel;
import in.socialtitli.retailerapp.Dashboard.Adapter.OrderHistoryAdapter;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.BillRowBinding;
import in.socialtitli.retailerapp.databinding.OrderHistoryRowBinding;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.MyViewHolder>{

    private Context mCon;
    private List<BillModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public BillAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<BillModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BillRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.bill_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final BillModel current = data.get(position);

        holder.binding.schemeNameTextVew.setText(current.getTitle());

        holder.binding.viewBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = current.getImage();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                mCon.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        BillRowBinding binding;

        public MyViewHolder(BillRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
