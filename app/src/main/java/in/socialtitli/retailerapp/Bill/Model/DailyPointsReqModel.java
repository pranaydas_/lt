package in.socialtitli.retailerapp.Bill.Model;

import com.google.gson.annotations.SerializedName;

public class DailyPointsReqModel {
    @SerializedName("retailerid")
    private String retailerid;

    @SerializedName("todaydate")
    private String todaydate;

    public DailyPointsReqModel(){}

    public DailyPointsReqModel(String retailerid, String todaydate) {
        this.retailerid = retailerid;
        this.todaydate = todaydate;
    }

    public String getRetailerid() {
        return retailerid;
    }

    public void setRetailerid(String retailerid) {
        this.retailerid = retailerid;
    }

    public String getTodaydate() {
        return todaydate;
    }

    public void setTodaydate(String todaydate) {
        this.todaydate = todaydate;
    }

    @Override
    public String toString() {
        return "DailyPointsModel{" +
                "retailerid='" + retailerid + '\'' +
                ", todaydate='" + todaydate + '\'' +
                '}';
    }
}
