package in.socialtitli.retailerapp.CounterBoy.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import in.socialtitli.retailerapp.CounterBoy.Model.CounterBoyModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.ActivityViewCounterBoyBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCounterBoyActivity extends AppCompatActivity {

    private ActivityViewCounterBoyBinding binding;
    private Context mCon;
    private String retailerId, counterboyId;
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_view_counter_boy);
        mCon = this;

        if (PreferenceUtil.getUser() != null) {
            retailerId = PreferenceUtil.getUser().getId();
        }

        counterboyId = getIntent().getStringExtra("counterBoyId");

        setUserDetails(retailerId,counterboyId);
    }

    private void setUserDetails(String retailerId, String counterboyId) {
        try {
            Call<CounterBoyModel> call = ApiClient.getNetworkService().fetchCounterBoyDetails(retailerId, counterboyId);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .content("Fetching Details")
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<CounterBoyModel>() {
                @Override
                public void onResponse(Call<CounterBoyModel> call, Response<CounterBoyModel> response) {
                    CounterBoyModel counterBoyModel = response.body();

                    Log.d("counterBoyModel", ""+counterBoyModel);
                    if (counterBoyModel.getName() != null){
                        binding.userNameTextView.setText(counterBoyModel.getName());
                    }
                    if (counterBoyModel.getMobile_no() != null){
                        binding.mobileNoTextView.setText(counterBoyModel.getMobile_no());
                    }
                    if (counterBoyModel.getDateOfBirth() != null){
                        binding.dobTextView.setText(counterBoyModel.getDateOfBirth());
                    }
                    if (counterBoyModel.getGstNumber() != null){
                        binding.gstTextView.setText(counterBoyModel.getGstNumber());
                    }
                    if (counterBoyModel.getAddressLine1() != null){
                        binding.addressLine1TextView.setText(counterBoyModel.getAddressLine1());
                    }
                    if (counterBoyModel.getAddressLine2() != null){
                        binding.addressLine2TextView.setText(counterBoyModel.getAddressLine2());
                    }
                    if (counterBoyModel.getAddressLine3() != null){
                        binding.addressLine3TextView.setText(counterBoyModel.getAddressLine3());
                    }
                    if (counterBoyModel.getPincode() != null) {
                        binding.pincodeTextView.setText(counterBoyModel.getPincode());
                    }
                    if (counterBoyModel.getCityName() != null && !(counterBoyModel.getCityName().equalsIgnoreCase(""))) {
                        binding.cityTextView.setText(counterBoyModel.getCityName());
                    }
                    if (counterBoyModel.getStateName() != null && !(counterBoyModel.getStateName().equalsIgnoreCase(""))) {
                        binding.stateTextView.setText(counterBoyModel.getStateName());
                    }

                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<CounterBoyModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        Log.d("callError", ""+t.getMessage());
                    }
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
            dialog.dismiss();
        }
    }
}