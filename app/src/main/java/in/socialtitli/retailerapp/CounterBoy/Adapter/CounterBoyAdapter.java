package in.socialtitli.retailerapp.CounterBoy.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.CounterBoy.Activity.EditCounterBoyActivity;
import in.socialtitli.retailerapp.CounterBoy.Activity.ViewCounterBoyActivity;
import in.socialtitli.retailerapp.CounterBoy.Model.CounterBoyModel;
import in.socialtitli.retailerapp.List.Activity.ElectricianDetailedActivity;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.CounterBoyRowBinding;

public class CounterBoyAdapter extends RecyclerView.Adapter<CounterBoyAdapter.MyViewHolder>{
    private Context mCon;
    private List<CounterBoyModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public CounterBoyAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<CounterBoyModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CounterBoyRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.counter_boy_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final CounterBoyModel current = data.get(position);

        holder.binding.counterboyNameTextVew.setText(current.getName());
        holder.binding.mobileNoTextVew.setText(current.getMobile_no());
        holder.binding.callCounterBoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = current.getMobile_no();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                mCon.startActivity(intent);
            }
        });

        holder.binding.counterboyListCardView.setOnClickListener(view -> {
            Intent intent = new Intent(mCon, ViewCounterBoyActivity.class);
            intent.putExtra("counterBoyId", current.getId());
            mCon.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CounterBoyRowBinding binding;

        public MyViewHolder(CounterBoyRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
