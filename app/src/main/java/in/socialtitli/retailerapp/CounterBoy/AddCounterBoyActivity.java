package in.socialtitli.retailerapp.CounterBoy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.CounterBoy.Activity.CounterBoyActivity;
import in.socialtitli.retailerapp.CounterBoy.Model.CounterBoyModel;
import in.socialtitli.retailerapp.Model.CityList;
import in.socialtitli.retailerapp.Model.StateList;
import in.socialtitli.retailerapp.MyProfile.Model.RetailerUpdateModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityAddCounterBoyBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCounterBoyActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityAddCounterBoyBinding binding;
    private MaterialDialog dialog;
    private String nameStr, address1Str, address2Str, address3Str, gstStr, pincodeStr, cityStr,
            dobStr, stateStr, today, cityOldStr="", mobStr, retailerId;
    private int statePos, cityPos, selId, cityIdSpinner;
    private List<String> states, cities;
    private List<Integer> statesIdDb, citiesIdDb;
    ArrayAdapter stateAdapter, cityAdapter;
    private List<StateList> stateList = new ArrayList<>();
    private List<CityList> cityList = new ArrayList<>();
    private int year, month, day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_counter_boy);
        mCon = this;

        if (PreferenceUtil.getUser() != null) {
            retailerId = PreferenceUtil.getUser().getId();
        }

        Objects.requireNonNull(getSupportActionBar()).setTitle("Add counter boy");

        loadState();

        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        today = "" + day + "-" + (month + 1) + "-" + year;

        binding.dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(mCon, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            binding.dob.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                        }
                    }, year, month, day);
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
                    datePickerDialog.show();
                }
            }
        });

        binding.addCounterBoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameStr = binding.nameEditText.getText().toString().trim();
                dobStr = binding.dob.getText().toString();
                mobStr = binding.mobEditText.getText().toString().trim();
                gstStr = binding.gstEditText.getText().toString().trim();
                address1Str = binding.address1EditText.getText().toString().trim();
                address2Str = binding.address2EditText.getText().toString().trim();
                address3Str = binding.address3EditText.getText().toString().trim();
                pincodeStr = binding.pincodeEditText.getText().toString().trim();

                validate();
            }
        });
    }

    private void loadState() {
        try {
            Call<List<StateList>> call = ApiClient.getNetworkService().fetchStates();

            call.enqueue(new Callback<List<StateList>>() {
                @Override
                public void onResponse(Call<List<StateList>> call, Response<List<StateList>> response) {
                    if (response.isSuccessful()) {
                        stateList = response.body();

                        if (stateList != null) {

                            states = new ArrayList<>();
                            statesIdDb = new ArrayList<>();
                            states.add("Select State");
                            statesIdDb.add(0);

                            for (StateList model : stateList) {
                                states.add(model.getStateName());
                                statesIdDb.add(model.getStateId());
                            }

                            stateAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, states);
                            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.stateSpinner.setAdapter(stateAdapter);

                            binding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    stateStr = binding.stateSpinner.getSelectedItem().toString();
                                    statePos = binding.stateSpinner.getSelectedItemPosition();

                                    selId = statesIdDb.get(statePos);

                                    loadCities(selId);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<StateList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCities(int stateId) {
        try {
            Call<List<CityList>> call = ApiClient.getNetworkService().fetchCities(stateId);

            call.enqueue(new Callback<List<CityList>>() {
                @Override
                public void onResponse(Call<List<CityList>> call, Response<List<CityList>> response) {
                    if (response.isSuccessful()) {
                        cityList = response.body();

                        if (cityList != null) {

                            cities = new ArrayList<>();
                            citiesIdDb = new ArrayList<>();
                            cities.add("Select City");
                            citiesIdDb.add(0);

                            for (CityList model : cityList) {
                                cities.add(model.getCityName());
                                citiesIdDb.add(model.getCityId());
                            }

                            cityAdapter = new ArrayAdapter<>(mCon, android.R.layout.simple_spinner_item, cities);
                            cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            binding.citySpinner.setAdapter(cityAdapter);

                            if (!cityOldStr.equalsIgnoreCase("")){
                                String compareValue = cityOldStr;
                                if (compareValue != null) {
                                    int spinnerPosition = cityAdapter.getPosition(compareValue);
                                    binding.citySpinner.setSelection(spinnerPosition);
                                }
                            }

                            binding.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    cityStr = binding.citySpinner.getSelectedItem().toString();
                                    cityPos = binding.citySpinner.getSelectedItemPosition();

                                    cityIdSpinner = citiesIdDb.get(cityPos);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    }
                }

                @Override
                public void onFailure(Call<List<CityList>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validate() {
        boolean isValidName = false, isValidGst = false, isValidAddress1 = false, isValidPincode = false, isValidCity = false,
                isValidDob = false, isValidMobile = false;

        //Name
        if (TextUtils.isEmpty(nameStr)) {
            binding.nameTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.nameTextLayout.setError(null);
            isValidName = true;
        }

        //Dob
        if (TextUtils.isEmpty(dobStr)) {
            binding.nameTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.nameTextLayout.setError(null);
            isValidDob = true;
        }

        //GST
        if (gstStr.length() > 0) {
            if (gstStr.length() < 15) {
                binding.gstTextLayout.setError(getResources().getString(R.string.enter_valid_gst));
            } else {
                isValidGst = true;
                binding.gstTextLayout.setError(null);
            }
        } else {
            binding.gstTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Address
        if (TextUtils.isEmpty(address1Str)) {
            binding.address1TextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        } else {
            binding.address1TextLayout.setError(null);
            isValidAddress1 = true;
        }

        //Pincode
        if (pincodeStr.length() > 0) {
            if (pincodeStr.length() < 6) {
                binding.pincodeTextLayout.setError(getResources().getString(R.string.enter_valid_pincode));
            } else {
                isValidPincode = true;
                binding.pincodeTextLayout.setError(null);
            }
        } else {
            binding.pincodeTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //Mobile
        if (mobStr.length() > 0) {
            if (mobStr.length() < 10) {
                binding.mobTextLayout.setError(getResources().getString(R.string.enter_valid_no));
            } else {
                isValidMobile = true;
                binding.mobTextLayout.setError(null);
            }
        } else {
            binding.mobTextLayout.setError(getResources().getString(R.string.field_cannot_be_empty));
        }

        //City
        if (cityPos != 0)
        {
            isValidCity = true;
        } else {
            Toast.makeText(mCon, "Select Valid City", Toast.LENGTH_SHORT).show();
        }

        if (isValidName && isValidDob && isValidGst && isValidAddress1 && isValidPincode && isValidCity && isValidMobile) {

            CounterBoyModel counterBoyModel = new CounterBoyModel("",nameStr, dobStr, mobStr, selId, cityIdSpinner, pincodeStr,
                    address1Str, address2Str, address3Str, gstStr, retailerId, "", "");
            addCounterBoy(counterBoyModel);
        }
    }

    private void addCounterBoy(CounterBoyModel counterBoyModel) {
        try {
            Call<String> call = ApiClient.getNetworkService().addCounterBoy(counterBoyModel);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .content(R.string.add_counter_boy_progress)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {
                            Toast.makeText(mCon, "Counter boy added successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(mCon, CounterBoyActivity.class));
                            finish();
                            dialog.dismiss();
                        }
                        else {
                            Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}