package in.socialtitli.retailerapp.CounterBoy.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import in.socialtitli.retailerapp.CounterBoy.Adapter.CounterBoyAdapter;
import in.socialtitli.retailerapp.CounterBoy.AddCounterBoyActivity;
import in.socialtitli.retailerapp.CounterBoy.Model.CounterBoyModel;
import in.socialtitli.retailerapp.List.Activity.AddElectricianActivity;
import in.socialtitli.retailerapp.List.Adapter.ElectricianListAdapter;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityAddCounterBoyBinding;
import in.socialtitli.retailerapp.databinding.ActivityCounterBoyBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CounterBoyActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityCounterBoyBinding binding;
    private MaterialDialog dialog;
    private CounterBoyAdapter counterBoyAdapter;
    private List<CounterBoyModel> counterBoyModelsList;
    private String retailerId;
    private Call<List<CounterBoyModel>> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_counter_boy);
        mCon = this;

        if (PreferenceUtil.getUser() != null) {
            retailerId = PreferenceUtil.getUser().getId();
        }

        binding.counterBoyListRecyclerView.setHasFixedSize(true);
        binding.counterBoyListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.counterBoyListRecyclerView.setLayoutManager(new LinearLayoutManager(mCon));

        counterBoyAdapter = new CounterBoyAdapter(mCon, retailerId);

        binding.addCounterBoyFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mCon, AddCounterBoyActivity.class);
                startActivity(intent);
            }
        });

        loadCounterBoyData();
    }

    private void loadCounterBoyData() {
        try {
            call = ApiClient.getNetworkService().fetchCounterBoy(retailerId);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.getting_counter_boy)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<List<CounterBoyModel>>() {
                @Override
                public void onResponse(Call<List<CounterBoyModel>> call, Response<List<CounterBoyModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            if (response.body() != null) {
                                counterBoyModelsList = response.body();

                                binding.errorLinear.setVisibility(View.GONE);

                                binding.totalTextView.setVisibility(View.VISIBLE);
                                binding.totalTextView.setText(getResources().getString(R.string.total_no_of_counter_boy) + " " + counterBoyModelsList.size());
                                binding.counterBoyListRecyclerView.setVisibility(View.VISIBLE);
                                counterBoyAdapter.addAll(counterBoyModelsList);
                                binding.counterBoyListRecyclerView.setAdapter(counterBoyAdapter);
                                counterBoyAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            } else {
                                binding.totalTextView.setVisibility(View.GONE);
                                binding.counterBoyListRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.data_not_found);
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            try {
                                binding.totalTextView.setVisibility(View.GONE);
                                binding.counterBoyListRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.no_data);
                                dialog.dismiss();
                                //Toast.makeText(mCon, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            binding.totalTextView.setVisibility(View.GONE);
                            binding.counterBoyListRecyclerView.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(R.string.data_not_found);
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        binding.totalTextView.setVisibility(View.GONE);
                        binding.counterBoyListRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<CounterBoyModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        binding.totalTextView.setVisibility(View.GONE);
                        binding.counterBoyListRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "loadElectricianData: ElectricianListFragment" + e.getMessage());
        }
    }
}