package in.socialtitli.retailerapp.CounterBoy.Model;

import com.google.gson.annotations.SerializedName;

public class CounterBoyModel {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("date_of_birth")
    private String dateOfBirth;

    @SerializedName("mobile_no")
    private String mobile_no;

    @SerializedName("state")
    private int stateid;

    @SerializedName("city")
    private int cityid;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("address_1")
    private String addressLine1;

    @SerializedName("address_2")
    private String addressLine2;

    @SerializedName("address_3")
    private String addressLine3;

    @SerializedName("gst_no")
    private String gstNumber;

    @SerializedName("retailerid")
    private String retailerid;

    @SerializedName("stateName")
    private String stateName;

    @SerializedName("cityName")
    private String cityName;

    public CounterBoyModel(){}

    public CounterBoyModel(String id, String name, String dateOfBirth, String mobile_no, int stateid, int cityid, String pincode, String addressLine1, String addressLine2, String addressLine3, String gstNumber, String retailerid, String stateName, String cityName) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.mobile_no = mobile_no;
        this.stateid = stateid;
        this.cityid = cityid;
        this.pincode = pincode;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.gstNumber = gstNumber;
        this.retailerid = retailerid;
        this.stateName = stateName;
        this.cityName = cityName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public int getStateid() {
        return stateid;
    }

    public void setStateid(int stateid) {
        this.stateid = stateid;
    }

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getRetailerid() {
        return retailerid;
    }

    public void setRetailerid(String retailerid) {
        this.retailerid = retailerid;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return "CounterBoyModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", mobile_no='" + mobile_no + '\'' +
                ", stateid=" + stateid +
                ", cityid=" + cityid +
                ", pincode='" + pincode + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", addressLine3='" + addressLine3 + '\'' +
                ", gstNumber='" + gstNumber + '\'' +
                ", retailerid='" + retailerid + '\'' +
                ", stateName='" + stateName + '\'' +
                ", cityName='" + cityName + '\'' +
                '}';
    }
}
