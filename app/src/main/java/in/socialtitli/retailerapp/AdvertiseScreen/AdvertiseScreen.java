package in.socialtitli.retailerapp.AdvertiseScreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;

import java.util.concurrent.TimeUnit;

import in.socialtitli.retailerapp.MainActivity;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.ActivityAdvertiseScreenBinding;

public class AdvertiseScreen extends AppCompatActivity {

    private ActivityAdvertiseScreenBinding binding;
    private Context mCon;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_advertise_screen);
        mCon = this;
        startTimer();

        binding.closeAdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                Intent it = new Intent(mCon, MainActivity.class);
                startActivity(it);
                finish();
            }
        });
    }

    private void startTimer() {

        //2 MINS = 120000

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(5000, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                binding.timerTextView.setText("" + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(500);

                startActivity(new Intent(mCon, MainActivity.class));
            }
        }.start();
    }
}