package in.socialtitli.retailerapp.Dashboard.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.List;

import in.socialtitli.retailerapp.Bill.Model.DailyPointsReqModel;
import in.socialtitli.retailerapp.Dashboard.Adapter.DailyPointsAdapter;
import in.socialtitli.retailerapp.Dashboard.Adapter.OrderHistoryAdapter;
import in.socialtitli.retailerapp.Dashboard.Model.DailyPointsModel;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.List.Adapter.ElectricianListAdapter;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityDailyElectricianPointsBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyElectricianPointsActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityDailyElectricianPointsBinding binding;
    private DailyPointsAdapter dailyPointsAdapter;
    private List<DailyPointsModel> dailyPointsModelsList;
    private String retailerIdStr, date;
    private MaterialDialog dialog;
    private DailyPointsReqModel dailyPointsReqModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_daily_electrician_points);
        mCon = this;

        retailerIdStr = getIntent().getStringExtra("retailerId");
        date = getIntent().getStringExtra("date");

        dailyPointsAdapter = new DailyPointsAdapter(mCon, retailerIdStr);

        binding.dailyPointsRecyclerView.setHasFixedSize(true);
        binding.dailyPointsRecyclerView.setLayoutManager(new LinearLayoutManager(mCon));
        binding.dailyPointsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        dailyPointsReqModel = new DailyPointsReqModel(retailerIdStr, date);
        loadDailyPoints(dailyPointsReqModel);
    }

    private void loadDailyPoints(DailyPointsReqModel dailyPointsReqModel) {
        try {
            Call<List<DailyPointsModel>> call = ApiClient.getNetworkService().fetchDailyPoints(dailyPointsReqModel);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title("Fetching Daily Points")
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<List<DailyPointsModel>>() {
                @Override
                public void onResponse(Call<List<DailyPointsModel>> call, Response<List<DailyPointsModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            if (response.body() != null) {

                                dailyPointsModelsList = response.body();

                                binding.errorLinear.setVisibility(View.GONE);
                                binding.dailyPointsRecyclerView.setVisibility(View.VISIBLE);
                                dailyPointsAdapter.addAll(dailyPointsModelsList);
                                binding.dailyPointsRecyclerView.setAdapter(dailyPointsAdapter);
                                dailyPointsAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            } else {
                                binding.dailyPointsRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.data_not_found);
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        /*dialogDisplay = false;
                        pageIndex = pageIndex + 10000;
                        loadElectricianData();*/

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            try {
                                binding.dailyPointsRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.no_data);
                                dialog.dismiss();
                                //Toast.makeText(mCon, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            binding.dailyPointsRecyclerView.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(R.string.data_not_found);
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        binding.dailyPointsRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<DailyPointsModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        binding.dailyPointsRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }
}