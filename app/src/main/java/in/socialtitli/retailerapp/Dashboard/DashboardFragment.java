package in.socialtitli.retailerapp.Dashboard;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.Dashboard.Activity.OrderHistoryActivity;
import in.socialtitli.retailerapp.Dashboard.Activity.PassbookActivity;
import in.socialtitli.retailerapp.Dashboard.Activity.SchemeOffersActivity;
import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.Dashboard.Model.RetailerPointsModel;
import in.socialtitli.retailerapp.Gifts.GiftFragment;
import in.socialtitli.retailerapp.Gifts.Model.GiftsModel;
import in.socialtitli.retailerapp.Model.ParentDetailsModel;
import in.socialtitli.retailerapp.Model.UserModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.FragmentDashboardBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    private FragmentDashboardBinding binding;
    private Context mCon;
    private UserModel userModel;
    private RetailerPointsModel retailerPointsModel;
    private String retailerId, parentMobile;

    public DashboardFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        mCon = getActivity();
        View view = binding.getRoot();

        userModel = PreferenceUtil.getUser();
        retailerId = userModel.getId();
        getParent(retailerId);

        binding.userNameTextView.setText(userModel.getName());

        binding.callAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = "18002091212";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", num, null));
                startActivity(intent);
            }
        });

        binding.callIso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!parentMobile.equalsIgnoreCase(""))
                {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", parentMobile, null));
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(mCon, "Retailer not yet mapped", Toast.LENGTH_SHORT).show();
                }
            }
        });

        loadPoints();

        binding.giftRedeemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GiftFragment giftFragment = new GiftFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment, giftFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        binding.passbookCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent passBookIntent = new Intent(mCon, PassbookActivity.class);
                startActivity(passBookIntent);
            }
        });

        binding.orderHistoryCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent orderHistoryIntent = new Intent(mCon, OrderHistoryActivity.class);
                startActivity(orderHistoryIntent);
            }
        });

        binding.schemeCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent schemeIntent = new Intent(mCon, SchemeOffersActivity.class);
                startActivity(schemeIntent);
            }
        });

        return view;
    }

    private void getParent(String retailerId) {
        try {
            Call<ParentDetailsModel> call = ApiClient.getNetworkService().fetchParent(retailerId);

            call.enqueue(new Callback<ParentDetailsModel>() {
                @Override
                public void onResponse(Call<ParentDetailsModel> call, Response<ParentDetailsModel> response) {
                    ParentDetailsModel parent = response.body();
                    Log.d("parent", ""+parent);
                    parentMobile = parent.getUsername();
                }

                @Override
                public void onFailure(Call<ParentDetailsModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        //Toast.makeText(mCon, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "sendOtp: OtpScreenActivity" + e.getMessage());
        }
    }

    private void loadPoints() {
        try {
            Call<RetailerPointsModel> call = ApiClient.getNetworkService().fetchPoints(retailerId);

            call.enqueue(new Callback<RetailerPointsModel>() {
                @Override
                public void onResponse(Call<RetailerPointsModel> call, Response<RetailerPointsModel> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {
                            retailerPointsModel = response.body();

                            Log.d("pointsApi", "" + retailerPointsModel);

                            if (retailerPointsModel != null){
                                binding.totalPoints.setText(retailerPointsModel.getTotal());
                                binding.redeemedPoints.setText(retailerPointsModel.getRedeem());
                                binding.balancePoints.setText(retailerPointsModel.getBalance());
                            }
                        }
                    } else {
                        Toast.makeText(mCon, "Failed to load points", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RetailerPointsModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, "Failed to load points", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "loadPoints: Points" + e.getMessage());
        }
    }
}