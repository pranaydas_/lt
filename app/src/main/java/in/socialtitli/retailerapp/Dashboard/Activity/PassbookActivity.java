package in.socialtitli.retailerapp.Dashboard.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.Dashboard.Model.RetailerPointsModel;
import in.socialtitli.retailerapp.Dashboard.Tabs.ElectricianPointsTab;
import in.socialtitli.retailerapp.Dashboard.Tabs.SchemePointsTab;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityPassbookBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassbookActivity extends AppCompatActivity {

    private ActivityPassbookBinding binding;
    private Context mCon;
    private String retailerIdStr;
    private List<PassbookModel> passbookModelList;
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_passbook);
        mCon = this;

        retailerIdStr = PreferenceUtil.getUser().getId();
        Objects.requireNonNull(getSupportActionBar()).setTitle("Passbook");

        PagerAdapter pagerAdapter = new PassbookActivity.FragmentAdapter(getSupportFragmentManager(), binding.tabLayout.getTabCount());
        binding.viewPager.setAdapter(pagerAdapter);

        binding.tabLayout.post(new Runnable() {
            @Override
            public void run() {
                binding.tabLayout.setupWithViewPager(binding.viewPager);
            }
        });
    }

    public class FragmentAdapter extends FragmentStatePagerAdapter {
        FragmentAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ElectricianPointsTab();

                case 1:
                    return new SchemePointsTab();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Daily Electrician Points";

                case 1:
                    return "Scheme Points";

                default:
                    return null;
            }
        }
    }
}