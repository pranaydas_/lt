package in.socialtitli.retailerapp.Dashboard.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityOrderHistoryBinding;
import in.socialtitli.retailerapp.databinding.ActivityViewOrderBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOrderActivity extends AppCompatActivity {

    private ActivityViewOrderBinding binding;
    private Context mCon;
    private String retailerIdStr, orderId;
    private OrderHistoryModel orderHistoryModel;
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_order);
        mCon = this;
        Objects.requireNonNull(getSupportActionBar()).setTitle("Order Details");

        retailerIdStr = PreferenceUtil.getUser().getId();
        orderId = getIntent().getStringExtra("orderId");

        getOrder(retailerIdStr, orderId);
    }

    private void getOrder(String retailerIdStr, String orderId) {
        try {
            Call<OrderHistoryModel> call = ApiClient.getNetworkService().getOrderDetails(retailerIdStr, orderId);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .content("Fetching Order Details")
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<OrderHistoryModel>() {
                @Override
                public void onResponse(Call<OrderHistoryModel> call, Response<OrderHistoryModel> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {
                            if (response.body() != null) {

                                orderHistoryModel = response.body();

                                binding.giftNameTextVew.setText(orderHistoryModel.getGiftName());
                                binding.giftPointsTextView.setText(orderHistoryModel.getPoints());
                                binding.qtyTextView.setText(orderHistoryModel.getQuantity());
                                binding.totalPointsTextView.setText(orderHistoryModel.getTotalPoints());
                                binding.reqReceivedTextView.setText(orderHistoryModel.getRequestReceived());
                                binding.reqRegisteredTextView.setText(orderHistoryModel.getRequestRegistered());
                                binding.reqApprovedTextView.setText(orderHistoryModel.getRequestApproved());

                                binding.giftProcuredTextView.setText(orderHistoryModel.getGiftProcured());
                                binding.giftDispatchedTextView.setText(orderHistoryModel.getGiftDispatched());
                                binding.courierIdTextView.setText(orderHistoryModel.getCourierId());
                                binding.dispatchedRemarkTextView.setText(orderHistoryModel.getDispatchedRemark());
                                binding.dispatchDateTextView.append("  "+orderHistoryModel.getDispatchedDate());

                                dialog.dismiss();
                            }
                            else {
                                Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }
                        else {
                            Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<OrderHistoryModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }
}