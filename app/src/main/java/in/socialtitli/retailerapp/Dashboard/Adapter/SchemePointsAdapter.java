package in.socialtitli.retailerapp.Dashboard.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.Dashboard.Model.SchemePointsModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.ElectricianPointsRowBinding;
import in.socialtitli.retailerapp.databinding.SchemePointsRowBinding;

public class SchemePointsAdapter extends RecyclerView.Adapter<SchemePointsAdapter.MyViewHolder>{
    private Context mCon;
    private List<SchemePointsModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public SchemePointsAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<SchemePointsModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SchemePointsRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.scheme_points_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SchemePointsModel current = data.get(position);

        holder.binding.schemeNameTextVew.setText(current.getScheme());
        holder.binding.schemePointsTextVew.setText(current.getPoint());
        holder.binding.schemePointsDate.setText(current.getCreated_date());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SchemePointsRowBinding binding;

        public MyViewHolder(SchemePointsRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
