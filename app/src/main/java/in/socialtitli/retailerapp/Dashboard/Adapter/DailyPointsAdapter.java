package in.socialtitli.retailerapp.Dashboard.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Activity.DailyElectricianPointsActivity;
import in.socialtitli.retailerapp.Dashboard.Model.DailyPointsModel;
import in.socialtitli.retailerapp.Dashboard.Model.PointsDateModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.DailyPointsRowBinding;
import in.socialtitli.retailerapp.databinding.ElectricianPointsRowBinding;

public class DailyPointsAdapter extends RecyclerView.Adapter<DailyPointsAdapter.MyViewHolder>{
    private Context mCon;
    private List<DailyPointsModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public DailyPointsAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<DailyPointsModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DailyPointsRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.daily_points_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.d("passListData", ""+data);
        final DailyPointsModel current = data.get(position);

        holder.binding.electricianNameTextVew.setText(current.getElectricianFullName());
        holder.binding.mobileNoTextVew.setText(current.getUsername());
        holder.binding.pointsTextView.setText(current.getPoint());

        holder.binding.callElectrician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = current.getUsername();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                mCon.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        DailyPointsRowBinding binding;

        public MyViewHolder(DailyPointsRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
