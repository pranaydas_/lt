package in.socialtitli.retailerapp.Dashboard.Model;

import com.google.gson.annotations.SerializedName;

public class PassbookModel {
    @SerializedName("point")
    private String point;

    @SerializedName("mobile_no")
    private String mobile_no;

    @SerializedName("id")
    private String id;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    public PassbookModel(){}

    public PassbookModel(String point, String mobile_no, String id, String first_name, String last_name) {
        this.point = point;
        this.mobile_no = mobile_no;
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    @Override
    public String toString() {
        return "PassbookModel{" +
                "point='" + point + '\'' +
                ", mobile_no='" + mobile_no + '\'' +
                ", id='" + id + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                '}';
    }
}
