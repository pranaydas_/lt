package in.socialtitli.retailerapp.Dashboard.Tabs;

import android.content.Context;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Adapter.ElectricianPointsAdapter;
import in.socialtitli.retailerapp.Dashboard.Adapter.SchemePointsAdapter;
import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.Dashboard.Model.SchemePointsModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.FragmentElectricianPointsTabBinding;
import in.socialtitli.retailerapp.databinding.FragmentSchemePointsTabBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchemePointsTab extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private FragmentSchemePointsTabBinding binding;
    private Context mCon;
    private String retailerId;
    private List<SchemePointsModel> schemePointsModels;
    private MaterialDialog dialog;
    private SchemePointsAdapter schemePointsAdapter;

    public SchemePointsTab() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_scheme_points_tab, container, false);
        mCon = getActivity();
        View view = binding.getRoot();

        retailerId = PreferenceUtil.getUser().getId();

        schemePointsAdapter = new SchemePointsAdapter(mCon, retailerId);

        binding.schemePointsListRecycler.setHasFixedSize(true);
        binding.schemePointsListRecycler.setLayoutManager(new LinearLayoutManager(mCon));
        binding.schemePointsListRecycler.setItemAnimator(new DefaultItemAnimator());

        binding.schemePointsSwipeRefresh.setOnRefreshListener(this);
        binding.schemePointsSwipeRefresh.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark, android.R.color.holo_red_dark);
        binding.schemePointsSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                binding.schemePointsSwipeRefresh.setRefreshing(true);
                loadSchemePoints();
            }
        });

        return view;
    }

    private void loadSchemePoints() {
        try {
            Call<List<SchemePointsModel>> call = ApiClient.getNetworkService().fetchSchemePoints(retailerId);

            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.getting_passbook)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();

            call.enqueue(new Callback<List<SchemePointsModel>>() {
                @Override
                public void onResponse(Call<List<SchemePointsModel>> call, Response<List<SchemePointsModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {

                            schemePointsModels = response.body();

                            if (schemePointsModels != null) {
                                binding.schemePointsListRecycler.setVisibility(View.VISIBLE);
                                binding.errorLinear.setVisibility(View.GONE);
                                schemePointsAdapter.addAll(schemePointsModels);
                                binding.schemePointsListRecycler.setAdapter(schemePointsAdapter);
                                binding.schemePointsSwipeRefresh.setRefreshing(false);
                                dialog.dismiss();
                            } else {
                                binding.schemePointsListRecycler.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(getResources().getString(R.string.no_data));
                                Toast.makeText(mCon, R.string.no_data, Toast.LENGTH_SHORT).show();
                                binding.schemePointsSwipeRefresh.setRefreshing(false);
                                dialog.dismiss();
                            }

                        } else {
                            binding.schemePointsListRecycler.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(getResources().getString(R.string.no_data));
                            Toast.makeText(mCon, R.string.no_data, Toast.LENGTH_SHORT).show();
                            binding.schemePointsSwipeRefresh.setRefreshing(false);
                            dialog.dismiss();
                        }
                    } else {
                        binding.schemePointsListRecycler.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(getResources().getString(R.string.no_data));
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        binding.schemePointsSwipeRefresh.setRefreshing(false);
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<SchemePointsModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        t.printStackTrace();
                        binding.schemePointsListRecycler.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(getResources().getString(R.string.no_data));
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        binding.schemePointsSwipeRefresh.setRefreshing(false);
                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "loadPoints: Points" + e.getMessage());
            dialog.dismiss();
        }
    }

    @Override
    public void onRefresh() {
        loadSchemePoints();
    }
}