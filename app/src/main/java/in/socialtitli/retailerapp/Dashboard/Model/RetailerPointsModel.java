package in.socialtitli.retailerapp.Dashboard.Model;

import com.google.gson.annotations.SerializedName;

public class RetailerPointsModel {
    @SerializedName("total")
    private String total;

    @SerializedName("redeem")
    private String redeem;

    @SerializedName("balance")
    private String balance;

    public RetailerPointsModel(){}

    public RetailerPointsModel(String total, String redeem, String balance) {
        this.total = total;
        this.redeem = redeem;
        this.balance = balance;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getRedeem() {
        return redeem;
    }

    public void setRedeem(String redeem) {
        this.redeem = redeem;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "RetailerPointsModel{" +
                "total='" + total + '\'' +
                ", redeem='" + redeem + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }
}
