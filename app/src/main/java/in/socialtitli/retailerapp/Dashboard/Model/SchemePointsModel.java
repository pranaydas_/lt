package in.socialtitli.retailerapp.Dashboard.Model;

import com.google.gson.annotations.SerializedName;

public class SchemePointsModel {
    @SerializedName("id")
    private String id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("point")
    private String point;

    @SerializedName("scheme")
    private String scheme;

    @SerializedName("created_date")
    private String created_date;

    public SchemePointsModel(){}

    public SchemePointsModel(String id, String user_id, String point, String scheme, String created_date) {
        this.id = id;
        this.user_id = user_id;
        this.point = point;
        this.scheme = scheme;
        this.created_date = created_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    @Override
    public String toString() {
        return "SchemePointsModel{" +
                "id='" + id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", point='" + point + '\'' +
                ", scheme='" + scheme + '\'' +
                ", created_date='" + created_date + '\'' +
                '}';
    }
}
