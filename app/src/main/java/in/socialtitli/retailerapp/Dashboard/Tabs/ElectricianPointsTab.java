package in.socialtitli.retailerapp.Dashboard.Tabs;

import android.content.Context;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Adapter.ElectricianPointsAdapter;
import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.Dashboard.Model.PointsDateModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityPassbookBinding;
import in.socialtitli.retailerapp.databinding.FragmentElectricianPointsTabBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ElectricianPointsTab extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private FragmentElectricianPointsTabBinding binding;
    private Context mCon;
    private String retailerId;
    private List<PassbookModel> passbookModelList;
    private List<PointsDateModel> pointsDateModels;
    private MaterialDialog dialog;
    private ElectricianPointsAdapter electricianPointsAdapter;

    public ElectricianPointsTab() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_electrician_points_tab, container, false);
        mCon = getActivity();
        View view = binding.getRoot();

        retailerId = PreferenceUtil.getUser().getId();

        electricianPointsAdapter = new ElectricianPointsAdapter(mCon, retailerId);

        binding.electricianPointsListRecycler.setHasFixedSize(true);
        binding.electricianPointsListRecycler.setLayoutManager(new LinearLayoutManager(mCon));
        binding.electricianPointsListRecycler.setItemAnimator(new DefaultItemAnimator());

        binding.electricianPointsSwipeRefresh.setOnRefreshListener(this);
        binding.electricianPointsSwipeRefresh.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark, android.R.color.holo_red_dark);
        binding.electricianPointsSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                binding.electricianPointsSwipeRefresh.setRefreshing(true);
                loadElectricianPoints();
            }
        });

        return view;
    }

    private void loadElectricianPoints() {
        try {
            Call<List<PointsDateModel>> call = ApiClient.getNetworkService().fetchElectricianPoints(retailerId);

            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.getting_passbook)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();

            call.enqueue(new Callback<List<PointsDateModel>>() {
                @Override
                public void onResponse(Call<List<PointsDateModel>> call, Response<List<PointsDateModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {

                            pointsDateModels = response.body();

                            if (pointsDateModels != null) {
                                binding.electricianPointsListRecycler.setVisibility(View.VISIBLE);
                                binding.errorLinear.setVisibility(View.GONE);
                                electricianPointsAdapter.addAll(pointsDateModels);
                                binding.electricianPointsListRecycler.setAdapter(electricianPointsAdapter);
                                binding.electricianPointsSwipeRefresh.setRefreshing(false);
                                dialog.dismiss();
                            } else {
                                binding.electricianPointsListRecycler.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(getResources().getString(R.string.no_data));
                                binding.electricianPointsSwipeRefresh.setRefreshing(false);
                                dialog.dismiss();
                            }

                        } else {
                            binding.electricianPointsListRecycler.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(getResources().getString(R.string.no_data));
                            binding.electricianPointsSwipeRefresh.setRefreshing(false);
                            dialog.dismiss();
                        }
                    } else {
                        binding.electricianPointsListRecycler.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(getResources().getString(R.string.no_data));
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        binding.electricianPointsSwipeRefresh.setRefreshing(false);
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<PointsDateModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        t.printStackTrace();
                        binding.electricianPointsListRecycler.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(getResources().getString(R.string.no_data));
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        binding.electricianPointsSwipeRefresh.setRefreshing(false);
                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "loadPoints: Points" + e.getMessage());
            dialog.dismiss();
        }
    }

    @Override
    public void onRefresh() {
        loadElectricianPoints();
    }
}