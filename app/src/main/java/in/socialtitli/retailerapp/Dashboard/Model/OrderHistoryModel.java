package in.socialtitli.retailerapp.Dashboard.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderHistoryModel implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("gift_id")
    private String giftId;

    @SerializedName("gift_ref_code")
    private String giftRefCode;

    @SerializedName("gift_name")
    private String giftName;

    @SerializedName("points")
    private String points;

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("total_points")
    private String totalPoints;

    @SerializedName("receiver_address")
    private String receiverAddress;

    @SerializedName("created_by")
    private String createdBy;

    @SerializedName("request_received")
    private String requestReceived;

    @SerializedName("request_registered")
    private String requestRegistered;

    @SerializedName("request_approved")
    private String requestApproved;

    @SerializedName("waiting_commercial_clearance")
    private String waitingCommercialClearance;

    @SerializedName("gift_procured")
    private String giftProcured;

    @SerializedName("gift_dispatched")
    private String giftDispatched;

    @SerializedName("courier_id")
    private String courierId;

    @SerializedName("dispatched_remark")
    private String dispatchedRemark;

    @SerializedName("refunded")
    private String refunded;

    @SerializedName("dispatched_date")
    private String dispatchedDate;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("tracking_url")
    private String trackingUrl;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("address_line_1")
    private String addressLine1;

    @SerializedName("address_line_2")
    private String addressLine2;

    @SerializedName("address_line_3")
    private String addressLine3;

    @SerializedName("city")
    private String city;

    @SerializedName("state")
    private String state;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("cancel_reason")
    private String cancelReason;

    public OrderHistoryModel(){}

    public OrderHistoryModel(String id, String userId, String giftId, String giftRefCode, String giftName, String points, String quantity, String totalPoints, String receiverAddress, String createdBy, String requestReceived, String requestRegistered, String requestApproved, String waitingCommercialClearance, String giftProcured, String giftDispatched, String courierId, String dispatchedRemark, String refunded, String dispatchedDate, String createdAt, String updatedAt, String trackingUrl, String firstName, String lastName, String addressLine1, String addressLine2, String addressLine3, String city, String state, String pincode, String cancelReason) {
        this.id = id;
        this.userId = userId;
        this.giftId = giftId;
        this.giftRefCode = giftRefCode;
        this.giftName = giftName;
        this.points = points;
        this.quantity = quantity;
        this.totalPoints = totalPoints;
        this.receiverAddress = receiverAddress;
        this.createdBy = createdBy;
        this.requestReceived = requestReceived;
        this.requestRegistered = requestRegistered;
        this.requestApproved = requestApproved;
        this.waitingCommercialClearance = waitingCommercialClearance;
        this.giftProcured = giftProcured;
        this.giftDispatched = giftDispatched;
        this.courierId = courierId;
        this.dispatchedRemark = dispatchedRemark;
        this.refunded = refunded;
        this.dispatchedDate = dispatchedDate;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.trackingUrl = trackingUrl;
        this.firstName = firstName;
        this.lastName = lastName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.cancelReason = cancelReason;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getGiftRefCode() {
        return giftRefCode;
    }

    public void setGiftRefCode(String giftRefCode) {
        this.giftRefCode = giftRefCode;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getRequestReceived() {
        return requestReceived;
    }

    public void setRequestReceived(String requestReceived) {
        this.requestReceived = requestReceived;
    }

    public String getRequestRegistered() {
        return requestRegistered;
    }

    public void setRequestRegistered(String requestRegistered) {
        this.requestRegistered = requestRegistered;
    }

    public String getRequestApproved() {
        return requestApproved;
    }

    public void setRequestApproved(String requestApproved) {
        this.requestApproved = requestApproved;
    }

    public String getWaitingCommercialClearance() {
        return waitingCommercialClearance;
    }

    public void setWaitingCommercialClearance(String waitingCommercialClearance) {
        this.waitingCommercialClearance = waitingCommercialClearance;
    }

    public String getGiftProcured() {
        return giftProcured;
    }

    public void setGiftProcured(String giftProcured) {
        this.giftProcured = giftProcured;
    }

    public String getGiftDispatched() {
        return giftDispatched;
    }

    public void setGiftDispatched(String giftDispatched) {
        this.giftDispatched = giftDispatched;
    }

    public String getCourierId() {
        return courierId;
    }

    public void setCourierId(String courierId) {
        this.courierId = courierId;
    }

    public String getDispatchedRemark() {
        return dispatchedRemark;
    }

    public void setDispatchedRemark(String dispatchedRemark) {
        this.dispatchedRemark = dispatchedRemark;
    }

    public String getRefunded() {
        return refunded;
    }

    public void setRefunded(String refunded) {
        this.refunded = refunded;
    }

    public String getDispatchedDate() {
        return dispatchedDate;
    }

    public void setDispatchedDate(String dispatchedDate) {
        this.dispatchedDate = dispatchedDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTrackingUrl() {
        return trackingUrl;
    }

    public void setTrackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    @Override
    public String toString() {
        return "OrderHistoryModel{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", giftId='" + giftId + '\'' +
                ", giftRefCode='" + giftRefCode + '\'' +
                ", giftName='" + giftName + '\'' +
                ", points='" + points + '\'' +
                ", quantity='" + quantity + '\'' +
                ", totalPoints='" + totalPoints + '\'' +
                ", receiverAddress='" + receiverAddress + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", requestReceived='" + requestReceived + '\'' +
                ", requestRegistered='" + requestRegistered + '\'' +
                ", requestApproved='" + requestApproved + '\'' +
                ", waitingCommercialClearance='" + waitingCommercialClearance + '\'' +
                ", giftProcured='" + giftProcured + '\'' +
                ", giftDispatched='" + giftDispatched + '\'' +
                ", courierId='" + courierId + '\'' +
                ", dispatchedRemark='" + dispatchedRemark + '\'' +
                ", refunded='" + refunded + '\'' +
                ", dispatchedDate='" + dispatchedDate + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", trackingUrl='" + trackingUrl + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", addressLine3='" + addressLine3 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", pincode='" + pincode + '\'' +
                ", cancelReason='" + cancelReason + '\'' +
                '}';
    }
}
