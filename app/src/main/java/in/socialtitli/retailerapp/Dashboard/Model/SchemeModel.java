package in.socialtitli.retailerapp.Dashboard.Model;

import com.google.gson.annotations.SerializedName;

public class SchemeModel {
    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("img")
    private String img;

    @SerializedName("pdf")
    private String pdf;

    @SerializedName("status")
    private String status;

    @SerializedName("created_date")
    private String created_date;

    @SerializedName("schemeimage")
    private String schemeimage;

    @SerializedName("schemepdf")
    private String schemepdf;

    public SchemeModel(){}

    public SchemeModel(String id, String title, String img, String pdf, String status, String created_date, String schemeimage, String schemepdf) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.pdf = pdf;
        this.status = status;
        this.created_date = created_date;
        this.schemeimage = schemeimage;
        this.schemepdf = schemepdf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getSchemeimage() {
        return schemeimage;
    }

    public void setSchemeimage(String schemeimage) {
        this.schemeimage = schemeimage;
    }

    public String getSchemepdf() {
        return schemepdf;
    }

    public void setSchemepdf(String schemepdf) {
        this.schemepdf = schemepdf;
    }

    @Override
    public String toString() {
        return "SchemeModel{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", img='" + img + '\'' +
                ", pdf='" + pdf + '\'' +
                ", status='" + status + '\'' +
                ", created_date='" + created_date + '\'' +
                ", schemeimage='" + schemeimage + '\'' +
                ", schemepdf='" + schemepdf + '\'' +
                '}';
    }
}
