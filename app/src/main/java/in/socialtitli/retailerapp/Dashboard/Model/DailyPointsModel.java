package in.socialtitli.retailerapp.Dashboard.Model;

import com.google.gson.annotations.SerializedName;

public class DailyPointsModel {
    @SerializedName("id")
    private String id;

    @SerializedName("electricianFullName")
    private String electricianFullName;

    @SerializedName("username")
    private String username;

    @SerializedName("point")
    private String point;

    @SerializedName("created_at")
    private String created_at;

    public DailyPointsModel(){}

    public DailyPointsModel(String id, String electricianFullName, String username, String point, String created_at) {
        this.id = id;
        this.electricianFullName = electricianFullName;
        this.username = username;
        this.point = point;
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getElectricianFullName() {
        return electricianFullName;
    }

    public void setElectricianFullName(String electricianFullName) {
        this.electricianFullName = electricianFullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "DailyPointsModel{" +
                "id='" + id + '\'' +
                ", electricianFullName='" + electricianFullName + '\'' +
                ", username='" + username + '\'' +
                ", point='" + point + '\'' +
                ", created_at='" + created_at + '\'' +
                '}';
    }
}
