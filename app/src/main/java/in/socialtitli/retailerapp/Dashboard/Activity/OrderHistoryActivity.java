package in.socialtitli.retailerapp.Dashboard.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.Dashboard.Adapter.OrderHistoryAdapter;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.List.Adapter.ElectricianListAdapter;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityOrderHistoryBinding;
import in.socialtitli.retailerapp.databinding.ActivityPassbookBinding;
import in.socialtitli.retailerapp.databinding.OrderHistoryRowBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryActivity extends AppCompatActivity{

    private ActivityOrderHistoryBinding binding;
    private Context mCon;
    private String retailerIdStr;
    private List<OrderHistoryModel> orderHistoryModelList;
    private MaterialDialog dialog;
    private OrderHistoryAdapter orderHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_history);
        mCon = this;

        retailerIdStr = PreferenceUtil.getUser().getId();
        Objects.requireNonNull(getSupportActionBar()).setTitle("Order History");

        orderHistoryAdapter = new OrderHistoryAdapter(mCon, retailerIdStr);

        binding.orderRecyclerView.setHasFixedSize(true);
        binding.orderRecyclerView.setLayoutManager(new LinearLayoutManager(mCon));
        binding.orderRecyclerView.setItemAnimator(new DefaultItemAnimator());

        loadOrders();
    }

    private void loadOrders() {
        try {
            Call<List<OrderHistoryModel>> call = ApiClient.getNetworkService().fetchOrderHistory(retailerIdStr);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.getting_order_history)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<List<OrderHistoryModel>>() {
                @Override
                public void onResponse(Call<List<OrderHistoryModel>> call, Response<List<OrderHistoryModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            if (response.body() != null) {

                                orderHistoryModelList = response.body();

                                Log.d("orderApi", ""+orderHistoryModelList);

                                binding.errorLinear.setVisibility(View.GONE);
                                binding.orderRecyclerView.setVisibility(View.VISIBLE);
                                orderHistoryAdapter.addAll(orderHistoryModelList);
                                binding.orderRecyclerView.setAdapter(orderHistoryAdapter);
                                orderHistoryAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            } else {
                                binding.orderRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.data_not_found);
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        /*dialogDisplay = false;
                        pageIndex = pageIndex + 10000;
                        loadElectricianData();*/

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            try {
                                binding.orderRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.no_data);
                                dialog.dismiss();
                                //Toast.makeText(mCon, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            binding.orderRecyclerView.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(R.string.data_not_found);
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        binding.orderRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<OrderHistoryModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        binding.orderRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }
}