package in.socialtitli.retailerapp.Dashboard.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Objects;

import in.socialtitli.retailerapp.Dashboard.Adapter.OrderHistoryAdapter;
import in.socialtitli.retailerapp.Dashboard.Adapter.SchemeOffersAdapter;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.Dashboard.Model.SchemeModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ActivityOrderHistoryBinding;
import in.socialtitli.retailerapp.databinding.ActivitySchemeOffersBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchemeOffersActivity extends AppCompatActivity {

    private ActivitySchemeOffersBinding binding;
    private Context mCon;
    private String retailerId;
    private List<SchemeModel> schemeModelsList;
    private MaterialDialog dialog;
    private GridLayoutManager manager;
    private SchemeOffersAdapter schemeOffersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_scheme_offers);
        mCon = this;

        retailerId = PreferenceUtil.getUser().getId();
        Objects.requireNonNull(getSupportActionBar()).setTitle("Scheme & Offers");

        schemeOffersAdapter = new SchemeOffersAdapter(mCon, retailerId);

        manager = new GridLayoutManager(mCon, 2);
        binding.schemeRecyclerView.setHasFixedSize(true);
        binding.schemeRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.schemeRecyclerView.setLayoutManager(manager);

        loadSchemes();
    }

    private void loadSchemes() {
        try {
            Call<List<SchemeModel>> call = ApiClient.getNetworkService().fetchSchemes();

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.getting_schemes)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<List<SchemeModel>>() {
                @Override
                public void onResponse(Call<List<SchemeModel>> call, Response<List<SchemeModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            if (response.body() != null) {

                                schemeModelsList = response.body();

                                binding.errorLinear.setVisibility(View.GONE);
                                binding.schemeRecyclerView.setVisibility(View.VISIBLE);
                                schemeOffersAdapter.addAll(schemeModelsList);
                                binding.schemeRecyclerView.setAdapter(schemeOffersAdapter);
                                schemeOffersAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            } else {
                                binding.schemeRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.data_not_found);
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        /*dialogDisplay = false;
                        pageIndex = pageIndex + 10000;
                        loadElectricianData();*/

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            try {
                                binding.schemeRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.no_data);
                                dialog.dismiss();
                                //Toast.makeText(mCon, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            binding.schemeRecyclerView.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(R.string.data_not_found);
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        binding.schemeRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<SchemeModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        binding.schemeRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }
}