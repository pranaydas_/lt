package in.socialtitli.retailerapp.Dashboard.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Activity.ViewOrderActivity;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.OrderHistoryRowBinding;


public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {
    private Context mCon;
    private List<OrderHistoryModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public OrderHistoryAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<OrderHistoryModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            Log.d("orderList", ""+data);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OrderHistoryRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.order_history_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final OrderHistoryModel current = data.get(position);

        holder.binding.giftNameTextVew.setText(current.getGiftName());
        holder.binding.giftPointsTextView.setText(current.getPoints());
        holder.binding.qtyTextView.setText(current.getQuantity());
        holder.binding.totalPointsTextView.setText(current.getTotalPoints());

        holder.binding.viewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mCon, ViewOrderActivity.class);
                it.putExtra("orderId", current.getId());
                mCon.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        OrderHistoryRowBinding binding;

        public MyViewHolder(OrderHistoryRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
