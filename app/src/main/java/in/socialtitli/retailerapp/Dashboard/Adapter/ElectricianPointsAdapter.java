package in.socialtitli.retailerapp.Dashboard.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Activity.DailyElectricianPointsActivity;
import in.socialtitli.retailerapp.Dashboard.Activity.PassbookActivity;
import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.Dashboard.Model.PassbookModel;
import in.socialtitli.retailerapp.Dashboard.Model.PointsDateModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.ElectricianPointsRowBinding;
import in.socialtitli.retailerapp.databinding.OrderHistoryRowBinding;

public class ElectricianPointsAdapter extends RecyclerView.Adapter<ElectricianPointsAdapter.MyViewHolder>{
    private Context mCon;
    private List<PointsDateModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public ElectricianPointsAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<PointsDateModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ElectricianPointsRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.electrician_points_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.d("passListData", ""+data);
        final PointsDateModel current = data.get(position);

        holder.binding.dateTextVew.setText(current.getPoints_date());
        holder.binding.pointsTextView.setText(current.getPoints());

        holder.binding.electricianPointsCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mCon, DailyElectricianPointsActivity.class);
                it.putExtra("retailerId", retailerId);
                it.putExtra("date", current.getPoints_date());
                mCon.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ElectricianPointsRowBinding binding;

        public MyViewHolder(ElectricianPointsRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
