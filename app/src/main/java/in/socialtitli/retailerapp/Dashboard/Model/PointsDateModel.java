package in.socialtitli.retailerapp.Dashboard.Model;

import com.google.gson.annotations.SerializedName;

public class PointsDateModel {
    @SerializedName("points_date")
    private String points_date;

    @SerializedName("points")
    private String points;

    public PointsDateModel(){}

    public PointsDateModel(String points_date, String points) {
        this.points_date = points_date;
        this.points = points;
    }

    public String getPoints_date() {
        return points_date;
    }

    public void setPoints_date(String points_date) {
        this.points_date = points_date;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "PointsDateModel{" +
                "points_date='" + points_date + '\'' +
                ", points='" + points + '\'' +
                '}';
    }
}
