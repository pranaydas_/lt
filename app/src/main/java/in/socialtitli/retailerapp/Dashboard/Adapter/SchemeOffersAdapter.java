package in.socialtitli.retailerapp.Dashboard.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Model.SchemeModel;
import in.socialtitli.retailerapp.Gifts.Adapter.GiftsAdapter;
import in.socialtitli.retailerapp.Gifts.Model.GiftsModel;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.databinding.GiftRowBinding;
import in.socialtitli.retailerapp.databinding.SchemeRowBinding;

public class SchemeOffersAdapter extends RecyclerView.Adapter<SchemeOffersAdapter.MyViewHolder>{
    private Context mCon;
    private List<SchemeModel> data;
    private LayoutInflater inflater;
    private String retailerId;

    public SchemeOffersAdapter(Context context, String retailerId) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
    }

    public void addAll(List<SchemeModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SchemeRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.scheme_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SchemeModel current = data.get(position);

        holder.binding.schemeTitle.append(current.getTitle());

        holder.binding.viewScheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!current.getImg().equals(""))
                {
                    String url = current.getSchemeimage();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    mCon.startActivity(i);
                }
                else if (!current.getPdf().equals(""))
                {
                    String url = current.getSchemepdf();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    mCon.startActivity(i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SchemeRowBinding binding;

        public MyViewHolder(SchemeRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
