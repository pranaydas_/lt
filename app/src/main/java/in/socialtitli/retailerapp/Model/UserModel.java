package in.socialtitli.retailerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("mobile_number")
    private String mobileNumber;

    @SerializedName("stateid")
    private String stateid;

    @SerializedName("cityid")
    private String cityid;

    @SerializedName("address_line_1")
    private String addressLine1;

    @SerializedName("address_line_2")
    private String addressLine2;

    @SerializedName("address_line_3")
    private String addressLine3;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("date_of_birth")
    private String dateOfBirth;

    @SerializedName("gst_number")
    private String gstNumber;

    @SerializedName("image")
    private String image;

    @SerializedName("user_total_balance")
    private String userTotalBalance;

    @SerializedName("user_redeem_balance")
    private String userRedeemBalance;

    @SerializedName("user_balance")
    private String userBalance;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("state")
    private String state;

    @SerializedName("city")
    private String city;

    @SerializedName("loginType")
    private String loginType;

    @SerializedName("code")
    private String code;

    @SerializedName("tmp_address")
    private String tmp_address;

    public UserModel(){}

    public UserModel(String id, String name, String firstName, String lastName, String mobileNumber, String stateid, String cityid, String addressLine1, String addressLine2, String addressLine3, String pincode, String dateOfBirth, String gstNumber, String image, String userTotalBalance, String userRedeemBalance, String userBalance, String createdAt, String state, String city, String loginType, String code, String tmp_address) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.stateid = stateid;
        this.cityid = cityid;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.pincode = pincode;
        this.dateOfBirth = dateOfBirth;
        this.gstNumber = gstNumber;
        this.image = image;
        this.userTotalBalance = userTotalBalance;
        this.userRedeemBalance = userRedeemBalance;
        this.userBalance = userBalance;
        this.createdAt = createdAt;
        this.state = state;
        this.city = city;
        this.loginType = loginType;
        this.code = code;
        this.tmp_address = tmp_address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserTotalBalance() {
        return userTotalBalance;
    }

    public void setUserTotalBalance(String userTotalBalance) {
        this.userTotalBalance = userTotalBalance;
    }

    public String getUserRedeemBalance() {
        return userRedeemBalance;
    }

    public void setUserRedeemBalance(String userRedeemBalance) {
        this.userRedeemBalance = userRedeemBalance;
    }

    public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTmp_address() {
        return tmp_address;
    }

    public void setTmp_address(String tmp_address) {
        this.tmp_address = tmp_address;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", stateid='" + stateid + '\'' +
                ", cityid='" + cityid + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", addressLine3='" + addressLine3 + '\'' +
                ", pincode='" + pincode + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", gstNumber='" + gstNumber + '\'' +
                ", image='" + image + '\'' +
                ", userTotalBalance='" + userTotalBalance + '\'' +
                ", userRedeemBalance='" + userRedeemBalance + '\'' +
                ", userBalance='" + userBalance + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", loginType='" + loginType + '\'' +
                ", code='" + code + '\'' +
                ", tmp_address='" + tmp_address + '\'' +
                '}';
    }
}
