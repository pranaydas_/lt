package in.socialtitli.retailerapp.Model;

import com.google.gson.annotations.SerializedName;

public class ParentDetailsModel {
    @SerializedName("id")
    private String isoId;

    @SerializedName("name")
    private String isoName;

    @SerializedName("username")
    private String username;

    public ParentDetailsModel(){}

    public ParentDetailsModel(String isoId, String isoName, String username) {
        this.isoId = isoId;
        this.isoName = isoName;
        this.username = username;
    }

    public String getIsoId() {
        return isoId;
    }

    public void setIsoId(String isoId) {
        this.isoId = isoId;
    }

    public String getIsoName() {
        return isoName;
    }

    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "ParentDetailsModel{" +
                "isoId='" + isoId + '\'' +
                ", isoName='" + isoName + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
