package in.socialtitli.retailerapp.Model;

import com.google.gson.annotations.SerializedName;

public class CityList {

    @SerializedName("id")
    private int cityId;

    @SerializedName("name")
    private String cityName;

    @SerializedName("state_id")
    private String stateid;

    public CityList() {}

    public CityList(int cityId, String cityName, String stateid) {
        this.cityId = cityId;
        this.cityName = cityName;
        this.stateid = stateid;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    @Override
    public String toString() {
        return "CityList{" +
                "cityId='" + cityId + '\'' +
                ", stateName='" + cityName + '\'' +
                ", stateId='" + stateid + '\'' +
                '}';
    }
}
