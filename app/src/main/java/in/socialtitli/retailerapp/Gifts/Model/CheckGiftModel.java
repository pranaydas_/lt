package in.socialtitli.retailerapp.Gifts.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckGiftModel implements Serializable {
    @SerializedName("retailerid")
    private String retailerId;

    @SerializedName("giftid")
    private String giftid;

    @SerializedName("qty")
    private String qty;

    @SerializedName("tmp_add")
    private String tmp_add;

    public CheckGiftModel(){}

    public CheckGiftModel(String retailerId, String giftid, String qty, String tmp_add) {
        this.retailerId = retailerId;
        this.giftid = giftid;
        this.qty = qty;
        this.tmp_add = tmp_add;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public String getGiftid() {
        return giftid;
    }

    public void setGiftid(String giftid) {
        this.giftid = giftid;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTmp_add() {
        return tmp_add;
    }

    public void setTmp_add(String tmp_add) {
        this.tmp_add = tmp_add;
    }

    @Override
    public String toString() {
        return "CheckGiftModel{" +
                "retailerId='" + retailerId + '\'' +
                ", giftid='" + giftid + '\'' +
                ", qty='" + qty + '\'' +
                ", tmp_add='" + tmp_add + '\'' +
                '}';
    }
}
