package in.socialtitli.retailerapp.Gifts.Model;

import com.google.gson.annotations.SerializedName;

public class GiftsModel {

    @SerializedName("id")
    private String id;

    @SerializedName("gift_ref_code")
    private String refCode;

    @SerializedName("gift_name")
    private String giftName;

    @SerializedName("points")
    private String points;

    @SerializedName("status")
    private String status;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("image")
    private String image;

    @SerializedName("gift_image")
    private String gift_image;

    public GiftsModel(){}

    public GiftsModel(String id, String refCode, String giftName, String points, String status, String created_at, String updated_at, String image, String gift_image) {
        this.id = id;
        this.refCode = refCode;
        this.giftName = giftName;
        this.points = points;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.image = image;
        this.gift_image = gift_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGift_image() {
        return gift_image;
    }

    public void setGift_image(String gift_image) {
        this.gift_image = gift_image;
    }

    @Override
    public String toString() {
        return "GiftsModel{" +
                "id='" + id + '\'' +
                ", refCode='" + refCode + '\'' +
                ", giftName='" + giftName + '\'' +
                ", points='" + points + '\'' +
                ", status='" + status + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", image='" + image + '\'' +
                ", gift_image='" + gift_image + '\'' +
                '}';
    }
}
