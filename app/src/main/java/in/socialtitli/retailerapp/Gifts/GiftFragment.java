package in.socialtitli.retailerapp.Gifts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Gifts.Adapter.GiftsAdapter;
import in.socialtitli.retailerapp.Gifts.Model.GiftsModel;
import in.socialtitli.retailerapp.List.Adapter.ElectricianListAdapter;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.FragmentGiftBinding;
import in.socialtitli.retailerapp.databinding.FragmentListBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftFragment extends Fragment {

    private Context mCon;
    private FragmentGiftBinding binding;
    private GiftsAdapter giftsAdapter;
    private List<GiftsModel> giftsModels;
    private String retailerIdStr, tmpAddressStr, permAddressStr;
    private GridLayoutManager manager;
    private Call<List<GiftsModel>> call;
    private MaterialDialog dialog;

    public GiftFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gift, container, false);
        View view = binding.getRoot();
        mCon = getActivity();
        retailerIdStr = PreferenceUtil.getUser().getId();
        tmpAddressStr = PreferenceUtil.getUser().getTmp_address();
        permAddressStr  = PreferenceUtil.getUser().getAddressLine1()+", "+PreferenceUtil.getUser().getAddressLine2()+
                ", "+PreferenceUtil.getUser().getAddressLine3();

        manager = new GridLayoutManager(mCon, 2);
        binding.giftRecyclerView.setHasFixedSize(true);
        binding.giftRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.giftRecyclerView.setLayoutManager(manager);

        giftsAdapter = new GiftsAdapter(mCon, retailerIdStr, tmpAddressStr, permAddressStr);

        binding.searchTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.searchView.getVisibility() == View.GONE) {
                    binding.searchView.setVisibility(View.VISIBLE);

                    binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return true;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            searchData(newText);
                            return false;
                        }
                    });

                } else {
                    binding.searchView.setVisibility(View.GONE);
                }

            }
        });

        loadGifts();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //pageIndex = 0;
            loadGifts();
            Log.d("check ", " onActivityResult: ");
        }
    }

    private void loadGifts() {
        try {
            call = ApiClient.getNetworkService().fetchingGifts();

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.getting_gifts)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<List<GiftsModel>>() {
                @Override
                public void onResponse(Call<List<GiftsModel>> call, Response<List<GiftsModel>> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == ResponseCodes.SUCCESS) {

                            if (response.body() != null) {
                                giftsModels = response.body();

                                binding.errorLinear.setVisibility(View.GONE);

                                binding.giftRecyclerView.setVisibility(View.VISIBLE);
                                giftsAdapter.addAll(giftsModels);
                                binding.giftRecyclerView.setAdapter(giftsAdapter);
                                giftsAdapter.notifyDataSetChanged();
                                dialog.dismiss();

                            } else {
                                binding.giftRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.data_not_found);
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }

                        } else if (response.code() == ResponseCodes.END_OF_LIST) {
                            try {
                                binding.giftRecyclerView.setVisibility(View.GONE);
                                binding.errorLinear.setVisibility(View.VISIBLE);
                                binding.errorTextView.setText(R.string.no_data);
                                dialog.dismiss();
                                //Toast.makeText(mCon, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            binding.giftRecyclerView.setVisibility(View.GONE);
                            binding.errorLinear.setVisibility(View.VISIBLE);
                            binding.errorTextView.setText(R.string.data_not_found);
                            Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        binding.giftRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<GiftsModel>> call, Throwable t) {
                    if (!call.isCanceled()) {
                        binding.giftRecyclerView.setVisibility(View.GONE);
                        binding.errorLinear.setVisibility(View.VISIBLE);
                        binding.errorTextView.setText(R.string.something_went_wrong);
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "loadGifts: Gift" + e.getMessage());
        }
    }

    private void searchData(String input) {
        List<GiftsModel> list = new ArrayList();

        for (GiftsModel model : giftsModels) {
            if (model.getGiftName().toLowerCase().contains(input.toLowerCase()) ||
                    model.getPoints().equals(input)) {
                list.add(model);
            }
        }

        giftsAdapter.filterList(list);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("check", "detach");
    }
}