package in.socialtitli.retailerapp.Gifts.Model;

import com.google.gson.annotations.SerializedName;

public class GiftResponseModel {
    @SerializedName("msg")
    private String msg;

    @SerializedName("json")
    private CheckGiftModel checkGiftModelJson;

    public GiftResponseModel(){}

    public GiftResponseModel(String msg, CheckGiftModel checkGiftModelJson) {
        this.msg = msg;
        this.checkGiftModelJson = checkGiftModelJson;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CheckGiftModel getCheckGiftModelJson() {
        return checkGiftModelJson;
    }

    public void setCheckGiftModelJson(CheckGiftModel checkGiftModelJson) {
        this.checkGiftModelJson = checkGiftModelJson;
    }

    @Override
    public String toString() {
        return "GiftResponseModel{" +
                "msg='" + msg + '\'' +
                ", checkGiftModelJson=" + checkGiftModelJson +
                '}';
    }
}
