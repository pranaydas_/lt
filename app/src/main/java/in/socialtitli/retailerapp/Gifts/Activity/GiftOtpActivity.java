package in.socialtitli.retailerapp.Gifts.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.concurrent.TimeUnit;

import in.socialtitli.retailerapp.Gifts.Model.CheckGiftModel;
import in.socialtitli.retailerapp.Login.OtpScreenActivity;
import in.socialtitli.retailerapp.Model.UserModel;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.databinding.ActivityGiftOtpBinding;
import in.socialtitli.retailerapp.databinding.ActivityOtpScreenBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftOtpActivity extends AppCompatActivity {

    private Context mCon;
    private ActivityGiftOtpBinding binding;
    private String retailerId, otpStr, username;
    private CountDownTimer countDownTimer;
    private CheckGiftModel jsonModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gift_otp);
        mCon = this;

        jsonModel = (CheckGiftModel) getIntent().getSerializableExtra("jsonModel");
        retailerId = PreferenceUtil.getUser().getId();
        username = PreferenceUtil.getUser().getMobileNumber();
        startTimer();

        binding.resendOtpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOtp();
            }
        });

        binding.verifyOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpStr = binding.otpEditText.getText().toString().trim();

                validate();
            }
        });
    }

    private void validate() {
        boolean isValidOtp = false;

        if (!TextUtils.isEmpty(otpStr)) {
            if (otpStr.length() < 4) {
                binding.otpTextLayout.setError(getResources().getString(R.string.enter_valid_otp));
            } else {
                binding.otpTextLayout.setError(null);
                isValidOtp = true;
            }
        } else {
            binding.otpTextLayout.setError(getResources().getString(R.string.cannot_be_empty));
        }

        if (isValidOtp) {
            redeemGift(username, otpStr, jsonModel);
        }
    }

    private void startTimer() {

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(180000, 1000) { // adjust the milli seconds here
            public void onTick(long millisUntilFinished) {
                binding.otpTimerTextView.setText("" + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {

                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(500);

                binding.otpTimerTextView.setVisibility(View.GONE);
                binding.resendOtpTextView.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    private void resendOtp() {
        try {
            Call<String> call = ApiClient.getNetworkService().loginUser(PreferenceUtil.getUser().getId());

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(mCon, response.body(), Toast.LENGTH_SHORT).show();
                        binding.resendOtpTextView.setVisibility(View.GONE);
                        binding.otpTimerTextView.setVisibility(View.VISIBLE);
                        startTimer();
                    } else {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "resendOtp: OtpScreenActivity" + e.getMessage());
        }
    }

    private void redeemGift(String username, String otp, CheckGiftModel model) {
        try {
            Call<String> call = ApiClient.getNetworkService().confirmBuyGift(username, otp, model);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(mCon, "Gift redeemed successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(mCon, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "sendOtp: OtpScreenActivity" + e.getMessage());
        }
    }

    private void exitOtpVerification() {
        new MaterialDialog.Builder(mCon)
                .title("Exit Otp Verification")
                .content("Are you sure you want to exit verification process")
                .positiveText("Yes")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .negativeText("No")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        exitOtpVerification();
    }
}