package in.socialtitli.retailerapp.Gifts.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import in.socialtitli.retailerapp.Dashboard.Model.OrderHistoryModel;
import in.socialtitli.retailerapp.Gifts.Activity.GiftOtpActivity;
import in.socialtitli.retailerapp.Gifts.Model.CheckGiftModel;
import in.socialtitli.retailerapp.Gifts.Model.GiftResponseModel;
import in.socialtitli.retailerapp.Gifts.Model.GiftsModel;
import in.socialtitli.retailerapp.List.Adapter.ElectricianListAdapter;
import in.socialtitli.retailerapp.List.Model.ElectricianListModel;
import in.socialtitli.retailerapp.Login.OtpScreenActivity;
import in.socialtitli.retailerapp.Network.ApiClient;
import in.socialtitli.retailerapp.R;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;
import in.socialtitli.retailerapp.Utils.ResponseCodes;
import in.socialtitli.retailerapp.databinding.ElectricianRowBinding;
import in.socialtitli.retailerapp.databinding.GiftRowBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftsAdapter extends RecyclerView.Adapter<GiftsAdapter.MyViewHolder>{
    private Context mCon;
    private List<GiftsModel> data;
    private LayoutInflater inflater;
    private String retailerId, tmpAddressStr, permAddressStr, userAddressStr;
    private MaterialDialog dialog;
    boolean isValidAddress;

    public GiftsAdapter(Context context, String retailerId, String tmpAddressStr, String permAddressStr) {
        mCon = context;
        data = new ArrayList<>();
        inflater = LayoutInflater.from(mCon);
        this.retailerId = retailerId;
        this.tmpAddressStr = tmpAddressStr;
        this.permAddressStr = permAddressStr;
    }

    public void addAll(List<GiftsModel> list) {
        if (data != null) {
            clear();
            data.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (data != null) {
            data.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GiftRowBinding binding = DataBindingUtil.inflate(inflater, R.layout.gift_row, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final GiftsModel current = data.get(position);
        int points = Integer.parseInt(current.getPoints());

        holder.binding.giftName.setText(current.getGiftName());
        holder.binding.giftPoints.setText(current.getPoints());
        holder.binding.giftTotal.setText("Total: 0");

        Glide
                .with(mCon)
                .asBitmap()
                .load(current.getGift_image())
                .skipMemoryCache(true)
                .into(holder.binding.giftImage);

        holder.binding.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int curr = Integer.parseInt(holder.binding.qtyEditText.getText().toString());
                int newQty = curr+1;
                int newTotal = newQty * points;
                holder.binding.qtyEditText.setText(newQty+"");
                holder.binding.giftTotal.setText("Total: "+newTotal);
            }
        });

        holder.binding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int curr = Integer.parseInt(holder.binding.qtyEditText.getText().toString());
                if(curr > 0) {
                    int newQty = curr - 1;
                    int newTotal = newQty * points;

                    holder.binding.qtyEditText.setText(newQty + "");
                    holder.binding.giftTotal.setText("Total: " + newTotal);
                }
            }
        });

        holder.binding.qtyEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String strQty = holder.binding.qtyEditText.getText().toString();

                if(!strQty.equals(""))
                {
                    int curr = Integer.parseInt(strQty);
                    if(curr > 0) {
                        int newTotal = curr * points;
                        holder.binding.giftTotal.setText("Total: " + newTotal);
                    }
                } else {
                    holder.binding.giftTotal.setText("Total: 0");
                }
            }
        });

        holder.binding.buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strQty = holder.binding.qtyEditText.getText().toString();
                String giftId = current.getId();

                if (tmpAddressStr.equalsIgnoreCase("") || tmpAddressStr == null)
                {
                    CheckGiftModel checkGiftModel = new CheckGiftModel(retailerId, giftId, strQty, "");
                    requestGiftReq(checkGiftModel);
                }
                else
                {
                    showAddressDialog(retailerId, giftId, strQty, permAddressStr,tmpAddressStr);
                }
            }
        });
    }

    private void showAddressDialog(String retailerId, String giftId, String strQty, String permAddressStr, String tmpAddressStr) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCon);
        View customLayout = inflater.inflate(R.layout.address_dialog, null);
        builder.setView(customLayout);

        MaterialButton submit = customLayout.findViewById(R.id.confirm);
        RadioGroup rdGrp = customLayout.findViewById(R.id.addressRadioGroup);
        RadioButton permAdd = customLayout.findViewById(R.id.pmAddRadioButton);
        RadioButton tempAdd = customLayout.findViewById(R.id.tmAppRadioButton);
        TextView errorText = customLayout.findViewById(R.id.genderErrorTextView);

        permAdd.setText(permAddressStr);
        tempAdd.setText(tmpAddressStr);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rdGrp.getCheckedRadioButtonId() == -1) {
                    errorText.setVisibility(View.VISIBLE);
                } else {
                    if (permAdd.isChecked()) {
                        userAddressStr = permAddressStr;
                        errorText.setVisibility(View.GONE);
                        isValidAddress = true;
                    } else if (tempAdd.isChecked()) {
                        userAddressStr = tmpAddressStr;
                        errorText.setVisibility(View.GONE);
                        isValidAddress = true;
                    }
                }

                if (isValidAddress)
                {
                    CheckGiftModel checkGiftModel = new CheckGiftModel(retailerId, giftId, strQty, userAddressStr);
                    requestGiftReq(checkGiftModel);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void requestGiftReq(CheckGiftModel checkGiftModel)
    {
        try {
            Call<GiftResponseModel> call = ApiClient.getNetworkService().redeemGift(checkGiftModel);

            /* if (dialogDisplay == true) {*/
            dialog = new MaterialDialog.Builder(mCon)
                    .title(R.string.checking_status)
                    .content(R.string.loading)
                    .canceledOnTouchOutside(false)
                    .progress(true, 0)
                    .widgetColorRes(R.color.colorPrimary)
                    .show();
            //}

            call.enqueue(new Callback<GiftResponseModel>() {
                @Override
                public void onResponse(Call<GiftResponseModel> call, Response<GiftResponseModel> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                            if (response.body() != null) {

                                String msg = response.body().getMsg();

                                if (msg.equals(ResponseCodes.OTP_SENT_STRING))
                                {
                                    Intent otp = new Intent(mCon, GiftOtpActivity.class);
                                    otp.putExtra("jsonModel", response.body().getCheckGiftModelJson());
                                    Toast.makeText(mCon, R.string.otp_sent, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                    mCon.startActivity(otp);
                                }
                                else if (msg.equals(ResponseCodes.NOT_ENOUGH_POINTS_STRING))
                                {
                                    Toast.makeText(mCon, R.string.not_enough_points, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                                else if (msg.equals(ResponseCodes.SOMETHING_WENT_WRONG_STRING))
                                {
                                    Toast.makeText(mCon, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                                else if (msg.equals(ResponseCodes.GIFT_NOT_AVAILABLE_STRING))
                                {
                                    Toast.makeText(mCon, R.string.gift_unavailable, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            } else {
                                Toast.makeText(mCon, R.string.data_not_found, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }
                    } else {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GiftResponseModel> call, Throwable t) {
                    if (!call.isCanceled()) {
                        Toast.makeText(mCon, R.string.check_internet, Toast.LENGTH_SHORT).show();

                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("check", "" + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void filterList(List<GiftsModel> list) {
        clear();
        data = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        GiftRowBinding binding;

        public MyViewHolder(GiftRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

}
