package in.socialtitli.retailerapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import java.util.Objects;

import in.socialtitli.retailerapp.Bill.Activity.BillActivity;
import in.socialtitli.retailerapp.Bill.Activity.BillUpload;
import in.socialtitli.retailerapp.ContactAdmin.FeedbackActivity;
import in.socialtitli.retailerapp.CounterBoy.Activity.CounterBoyActivity;
import in.socialtitli.retailerapp.CounterBoy.AddCounterBoyActivity;
import in.socialtitli.retailerapp.Dashboard.DashboardFragment;
import in.socialtitli.retailerapp.Gifts.GiftFragment;
import in.socialtitli.retailerapp.List.ElectricianListFragment;
import in.socialtitli.retailerapp.Login.LoginActivity;
import in.socialtitli.retailerapp.Model.UserModel;
import in.socialtitli.retailerapp.MyProfile.ProfileFragment;
import in.socialtitli.retailerapp.Qrcode.Activity.QrCodeActivity;
import in.socialtitli.retailerapp.Utils.ForceUpdateAsync;
import in.socialtitli.retailerapp.Utils.PreferenceUtil;

import static android.content.pm.PackageManager.GET_META_DATA;

public class MainActivity extends AppCompatActivity {

    private Context mCon;
    UserModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCon = this;
        forceUpdate();

        if(PreferenceUtil.isUserLoggedIn())
        {
            if (PreferenceUtil.getUser() != null) {
                BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigation);
                bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
                Objects.requireNonNull(getSupportActionBar()).setTitle("Dashboard");
                getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new DashboardFragment()).commit();
            }
        }
        else if (!PreferenceUtil.isUserLoggedIn())
        {
            startActivity(new Intent(mCon, LoginActivity.class));
            finish();
        }

        Objects.requireNonNull(getSupportActionBar()).setElevation(0);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selFrag = null;

                    switch (menuItem.getItemId()) {
                        case R.id.bottom_dashboard:
                            selFrag = new DashboardFragment();
                            Objects.requireNonNull(getSupportActionBar()).setTitle("Dashboard");
                            break;

                        case R.id.bottom_list:
                            selFrag = new ElectricianListFragment();
                            Objects.requireNonNull(getSupportActionBar()).setTitle("List of Electricians");
                            break;

                        case R.id.bottom_gifts:
                            selFrag = new GiftFragment();
                            Objects.requireNonNull(getSupportActionBar()).setTitle("Gifts");
                            break;

                        case R.id.bottom_profile:
                            selFrag = new ProfileFragment();
                            Objects.requireNonNull(getSupportActionBar()).setTitle("Profile");
                            break;

                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, selFrag).commit();

                    return true;
                }
            };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_qr) {
            startActivity(new Intent(mCon, QrCodeActivity.class));
        } else if (id == R.id.menu_counter_boy) {
            startActivity(new Intent(mCon, CounterBoyActivity.class));
        } else if (id == R.id.menu_bill) {
            startActivity(new Intent(mCon, BillActivity.class));
        } else if (id == R.id.menu_feedback) {
            startActivity(new Intent(mCon, FeedbackActivity.class));
        } else if (id == R.id.menu_logout) {
            new MaterialDialog.Builder(mCon)
                    .title(R.string.logout)
                    .content(R.string.logout_dialog)
                    .canceledOnTouchOutside(false)
                    .positiveText("Yes")
                    .negativeText("No")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            PreferenceUtil.clearAll();
                            startActivity(new Intent(mCon, LoginActivity.class));
                            finish();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                //.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.nav_host_fragment, fragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            new MaterialDialog.Builder(mCon)
                    .title("Exit")
                    .content("Are you sure, you want to exit")
                    .positiveText("Yes")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        if (packageManager != null) {
            PackageInfo packageInfo = null;
            try {
                packageInfo = packageManager.getPackageInfo(getPackageName(), GET_META_DATA);

                Log.d("check", String.valueOf(packageInfo));

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (packageInfo != null) {
                String currentVersion = packageInfo.versionName;

                Log.d("check", currentVersion);

                new ForceUpdateAsync(currentVersion, mCon).execute();
            }
        }
    }
}